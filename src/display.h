#ifndef _INCLUDED_DISPLAY_H_
#	define _INCLUDED_DISPLAY_H_

#include "esp_system.h"
#include "font.h"

esp_err_t display_init();
void display_invert(int inv);
void display_draw_string_fb(const char *s, const struct font *font, unsigned int x, unsigned int y, int inv);
void display_draw_string(const char *s, const struct font *font, unsigned int x, unsigned int y, int inv);
void display_fill_area_fb(unsigned int x, unsigned int y, unsigned int w, unsigned int h, int v);
void display_fill_area(unsigned int x, unsigned int y, unsigned int w, unsigned int h, int v);
void display_update_area(unsigned int x, unsigned int y, unsigned int w, unsigned int h);

#endif /* ndef _INCLUDED_DISPLAY_H_ */

/* vim: set ts=4 sw=4 noet nowrap: */
