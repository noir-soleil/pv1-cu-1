#ifndef __INCLUDED_CRC14_H__
#define __INCLUDED_CRC14_H__

#include <stdint.h>

/* POLY: (0x82608edb; 0x104c11db7), len: 32, data: 8bit, init: 0x00000000, RefIn: N, RefOut: N, XorOut: 0x00000000 */

extern uint32_t crc32_table[256];

static inline uint32_t crc32_byte(uint32_t crc, unsigned char data)
{
	return (crc << 8) ^ crc32_table[((crc >> 24) ^ data) & 0xFF];
}

static inline uint32_t crc32_calc(uint32_t crc, const void *data, size_t len)
{
	const unsigned char *m = (const unsigned char*)data;
	while (len--) crc = crc32_byte(crc, *m++);
	return crc;
}

#endif /* ndef __INCLUDED_CRC14_H__ */

