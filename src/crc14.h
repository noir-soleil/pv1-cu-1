#ifndef __INCLUDED_CRC14_H__
#define __INCLUDED_CRC14_H__

#include <stdint.h>

/* POLY: (0x372b; 0x6e57), len: 14, data: 7bit, init: 0x0000, RefIn: Y, RefOut: Y, XorOut: 0x0000 */

#define CRC14_7BIT_INIT 0x0000

extern uint16_t crc14_7bit_table[128];

static inline uint16_t crc14_7bit_byte(uint16_t crc, unsigned char data)
{
	return (crc >> 7) ^ crc14_7bit_table[(crc ^ data) & 0x7F];
}

static inline uint16_t crc14_7bit_calc(uint16_t crc, const void *data, size_t len)
{
	const unsigned char *m = (const unsigned char*)data;
	while (len--) crc = crc14_7bit_byte(crc, *m++);
	return crc;
}

#endif /* ndef __INCLUDED_CRC14_H__ */

