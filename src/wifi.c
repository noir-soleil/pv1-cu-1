#include <wifi.h>
#include "sdkconfig.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp_log.h"
#include "esp_event.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs.h"


/*

#include "freertos/task.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include <sys/param.h>
#include "esp_netif.h"

#include <stdio.h>
#include <stdlib.h>
#include "freertos/queue.h"
*/

static int wifi_wsfds[CONFIG_LWIP_MAX_LISTENING_TCP] = {0};
static httpd_handle_t wifi_httpServer;
static httpd_close_func_t wifi_prevCloseFunc = NULL;

esp_ip4_addr_t wifi_sta_ip4 = {0};

static const char *TAGhttp = "wifi:HTTP";
static const char *TAGws = "wifi:WS";

static const char *TAG = "wifi:cmn";
static const char *TAGap = "wifi:softAP";
static const char *TAGWst = "wifi:STA";

static char wifi_msg_buf[256] = {0};

static wifi_config_t wifi_cfg_ap = {
	.ap = {
		.ssid = "",
		.ssid_len = 0,
		.channel = CONFIG_AP_WIFI_CHANNEL,
		.password = CONFIG_AP_WIFI_PASSWORD,
		.max_connection = CONFIG_AP_MAX_STA_CONN,
		.authmode = WIFI_AUTH_WPA_WPA2_PSK
	},
};

static wifi_config_t wifi_cfg_sta = {
	.sta = {
		.ssid = "PVCU-rangeExt",
		.password = "12345678",
		.threshold.authmode = WIFI_AUTH_WPA2_PSK,
		.threshold.rssi = -78
	},
};

const wifi_ap_config_t *wifi_get_ap_config(void)
{
	return &wifi_cfg_ap.ap;
}

const wifi_sta_config_t *wifi_get_sta_config(void)
{
	return &wifi_cfg_sta.sta;
}

static void wifi_send_to_ws(const char *str, httpd_ws_type_t type, bool final)
{
	httpd_ws_frame_t ws_pkt;
	memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
	ws_pkt.payload = (uint8_t*)str;
	ws_pkt.len = strlen(str);
	ws_pkt.type = type;
	if (!final) {
		ws_pkt.fragmented = true;
		ws_pkt.final = false;
	} else {
		ws_pkt.final = true;
	}

	for(size_t idx = 0; idx < sizeof(wifi_wsfds)/sizeof(wifi_wsfds[0]); ++idx) {
		if (!wifi_wsfds[idx]) continue;
		httpd_ws_send_frame_async(wifi_httpServer, wifi_wsfds[idx], &ws_pkt);
	}
}

static esp_err_t wifi_uri_wifi_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGhttp, "executing %s() on CORE%i", __func__, xPortGetCoreID());
	extern const char start_wifi_html[] asm("_binary_wifi_html_start");
	extern const char end_wifi_html[] asm("_binary_wifi_html_end");
	httpd_resp_send(req, start_wifi_html, end_wifi_html - start_wifi_html - 1 /* omit '\0' */);
	return ESP_OK;
}

static const httpd_uri_t wifi_uri_wifi = {
	.uri = "/wifi",
	.method = HTTP_GET,
	.handler = wifi_uri_wifi_handler,
	.user_ctx = NULL
};

static esp_err_t wifi_uri_wifi_js_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGhttp, "executing %s() on CORE%i", __func__, xPortGetCoreID());
	extern const char start_wifi_js[] asm("_binary_wifi_js_start");
	extern const char end_wifi_js[] asm("_binary_wifi_js_end");
	httpd_resp_set_type(req, "text/javascript");
	httpd_resp_send(req, start_wifi_js, end_wifi_js - start_wifi_js - 1 /* omit '\0' */);
	return ESP_OK;
}

static const httpd_uri_t wifi_uri_wifi_js = {
	.uri = "/wifi.js",
	.method = HTTP_GET,
	.handler = wifi_uri_wifi_js_handler,
	.user_ctx = NULL
};

static const char* auth_mode_str(int authmode)
{
	switch (authmode) {
		case WIFI_AUTH_OPEN:
			return "OPEN";
		case WIFI_AUTH_WEP:
			return "WEP";
		case WIFI_AUTH_WPA_PSK:
			return "WPA PSK";
		case WIFI_AUTH_WPA2_PSK:
			return "WPA2 PSK";
		case WIFI_AUTH_WPA_WPA2_PSK:
			return "WPA/WPA2 PSK";
		case WIFI_AUTH_WPA2_ENTERPRISE:
			return "WPA2 ENTERPRISE";
		case WIFI_AUTH_WPA3_PSK:
			return "WPA3 PSK";
		case WIFI_AUTH_WPA2_WPA3_PSK:
			return "WPA2/WPA3 PSK";
	}
	return "UNKNOWN";
}

static const char *cipher_str(int cipher)
{
	switch (cipher) {
		case WIFI_CIPHER_TYPE_NONE:
			return "NONE";
		case WIFI_CIPHER_TYPE_WEP40:
			return "WEP40";
		case WIFI_CIPHER_TYPE_WEP104:
			return "WEP104";
		case WIFI_CIPHER_TYPE_TKIP:
			return "TKIP";
		case WIFI_CIPHER_TYPE_CCMP:
			return "CCMP";
		case WIFI_CIPHER_TYPE_TKIP_CCMP:
			return "TKIP,CCMP";
	}
	return "UNKNOWN";
}

static void wifi_scan_start_delayed_task(void *parameters)
{
	vTaskDelay((uint32_t)parameters / portTICK_RATE_MS);
	for(size_t idx = 0; idx < sizeof(wifi_wsfds)/sizeof(wifi_wsfds[0]); ++idx) {
		if (wifi_wsfds[idx]) {
			esp_wifi_scan_start(NULL, false);
			break;
		}
	}
	vTaskDelete(NULL);
}

static void wifi_scan_start_delayed(uint32_t delay)
{
	xTaskCreatePinnedToCore(&wifi_scan_start_delayed_task, "WiFi_scan_start_task", 1024, (void*)delay,
			tskIDLE_PRIORITY, NULL, xPortGetCoreID());
}

static void wifi_scan_done_handler(wifi_event_sta_scan_done_t* e)
{
	ESP_LOGI(TAGWst, "WIFI scan done status: %d, num: %d, id: %d", e->status, e->number, e->scan_id);
	uint16_t number = 0;
	ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&number));
	ESP_LOGI(TAGWst, "Total APs scanned: %u", (unsigned int)number);
	snprintf(wifi_msg_buf, sizeof(wifi_msg_buf), "%u\n", (unsigned int)number);
	wifi_send_to_ws(wifi_msg_buf, HTTPD_WS_TYPE_TEXT, false);
	if (!number) {
		wifi_send_to_ws("", HTTPD_WS_TYPE_CONTINUE, true);
		return;
	}
	wifi_ap_record_t* ap_records = (wifi_ap_record_t*)malloc(number * sizeof(wifi_ap_record_t));
	esp_err_t ret = ESP_OK;
	if (!ap_records || (ret = esp_wifi_scan_get_ap_records(&number, ap_records)) != ESP_OK) {
		if (!ap_records) {
			number = 0;
			/* FIXME: Will that free internal records? */
			esp_wifi_scan_get_ap_records(&number, ap_records);
			ESP_LOGE(TAGWst, "Unable to allocate wifi_ap_record_t array");
		} else free(ap_records);
#if (ESP_IDF_VERSION_MAJOR >= 5)
		ESP_ERROR_CHECK(esp_wifi_clear_ap_list());
#endif
		ESP_ERROR_CHECK(ret);
		wifi_send_to_ws("", HTTPD_WS_TYPE_CONTINUE, true);
		return;
	}
	for (unsigned int i = 0; i < number; i++) {
		snprintf(wifi_msg_buf, sizeof(wifi_msg_buf),
				"[%02u] " MACSTR " '%s' (%d) %s, ch: %d\n",
				i, MAC2STR(ap_records[i].bssid), ap_records[i].ssid,
				ap_records[i].rssi, auth_mode_str(ap_records[i].authmode),
				ap_records[i].primary);
		wifi_send_to_ws(wifi_msg_buf, HTTPD_WS_TYPE_CONTINUE, false);

		ESP_LOGI(TAGWst, "[%02u] " MACSTR " '%s' (%d) auth: %s, ch: %d",
				i, MAC2STR(ap_records[i].bssid), ap_records[i].ssid,
				ap_records[i].rssi, auth_mode_str(ap_records[i].authmode),
				ap_records[i].primary);

		wifi_mode_t m;
		if (esp_wifi_get_mode(&m) == ESP_OK && m != WIFI_MODE_STA
				&& !strcmp((char*)ap_records[i].ssid, (char*)wifi_cfg_sta.sta.ssid)
				&& ap_records[i].authmode >= wifi_cfg_sta.sta.threshold.authmode
				&& ap_records[i].rssi >= wifi_cfg_sta.sta.threshold.rssi) {
			//ESP_LOGI(TAGWst, "Switching to STA mode, connecting to '%s'", ap_records[i].ssid);
			//ESP_ERROR_CHECK(esp_wifi_connect());
		}
		/*
		   if (ap_records[i].authmode != WIFI_AUTH_WEP) {
		   print_cipher_type(ap_records[i].pairwise_cipher, ap_records[i].group_cipher);
		   }
		   */
	}
	wifi_send_to_ws("", HTTPD_WS_TYPE_CONTINUE, true);
	free(ap_records);
}

static void wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
	ESP_LOGI(TAG, "executing %s() on CORE%i", __func__, xPortGetCoreID());
	if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STACONNECTED) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_AP_STACONNECTED");
		/* wifi_event_ap_staconnected_t */
		wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
		ESP_LOGI(TAGap, "station " MACSTR " join, AID=%d", MAC2STR(event->mac), event->aid);
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STADISCONNECTED) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_AP_STADISCONNECTED");
		/* wifi_event_ap_stadisconnected_t */
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAGap, "station " MACSTR " leave, AID=%d", MAC2STR(event->mac), event->aid);
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_WIFI_READY) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_WIFI_READY");
		/* n/a */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_SCAN_DONE) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_SCAN_DONE");
		/* wifi_event_sta_scan_done_t */
		wifi_scan_done_handler((wifi_event_sta_scan_done_t*) event_data);
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_START");
		esp_wifi_connect();
		/* n/a */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_STOP) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_STOP");
		/* n/a */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_CONNECTED");
		/* wifi_event_sta_connected_t */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_DISCONNECTED");
		wifi_sta_ip4.addr = 0;
		esp_wifi_connect();
		/* wifi_event_sta_disconnected_t */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_AUTHMODE_CHANGE) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_AUTHMODE_CHANGE");
		/* wifi_event_sta_authmode_change_t */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_WPS_ER_SUCCESS) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_WPS_ER_SUCCESS");
		/* n/a */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_WPS_ER_FAILED) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_WPS_ER_FAILED");
		/* wifi_event_sta_wps_fail_reason_t */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_WPS_ER_TIMEOUT) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_WPS_ER_TIMEOUT");
		/* n/a */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_WPS_ER_PIN) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_STA_WPS_ER_PIN");
		/* wifi_event_sta_wps_er_pin_t */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_START) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_AP_START");
		/* n/a */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STOP) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_AP_STOP");
		/* n/a */
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_PROBEREQRECVED) {
		ESP_LOGI(TAG, "WIFI_EVENT WIFI_EVENT_AP_PROBEREQRECVED");
		/* wifi_event_ap_probe_req_rx_t */
	} else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
		ESP_LOGI(TAG, "IP_EVENT IP_EVENT_STA_GOT_IP");
		/* ip_event_got_ip_t */
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
		wifi_sta_ip4 = event->ip_info.ip;
	} else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_LOST_IP) {
		ESP_LOGI(TAG, "IP_EVENT IP_EVENT_STA_LOST_IP");
		wifi_sta_ip4.addr = 0;
		/* n/a */
	} else if (event_base == IP_EVENT && event_id == IP_EVENT_AP_STAIPASSIGNED) {
		ESP_LOGI(TAG, "IP_EVENT IP_EVENT_AP_STAIPASSIGNED");
		/* n/a */
	} else if (event_base == IP_EVENT && event_id == IP_EVENT_GOT_IP6) {
		ESP_LOGI(TAG, "IP_EVENT IP_EVENT_GOT_IP6");
		/* ip_event_got_ip6_t */
    }
}

static size_t process_SSID(const char *ssid, char *out, size_t max_len)
{
	char buf[65];
	strncpy(buf, ssid, sizeof(buf) - 1);
	buf[sizeof(buf) - 1] = '\0';
	char *pos = buf;
	unsigned int len = strlen(buf);
	while (*pos) {
		if (*pos == '%') {
			++pos;
			if (*pos == '%') {
				/* "%%" => "%" */
				char *c = pos + 1;
				do {
					*(c - 1) = *c;
				} while (*c++);
				len -= 1;
			} else if (*pos == 'm') {
				/* Move the rest of the string further back */
				char *c = buf + len;
				do {
					char *t = c + (12 - 2);
					if ((t - buf) < (sizeof(buf) - 1)) {
						*t = *c;
					} else if ((t - buf) == (sizeof(buf) - 1)) {
						*t = '\0';
						if (*c) len -= 1;
					} else {
						if (*c) len -= 1;
					}
				} while (--c != pos);
				uint8_t mac[8];
				ESP_ERROR_CHECK(esp_read_mac(mac, ESP_MAC_WIFI_SOFTAP));
				--pos;
				/* Write MAC in hex into string */
				for (int i = 0; i < 12; ++i) {
					uint8_t v = (mac[i >> 1] >> ((i & 1) ? 0 : 4)) & 0x0F;
					if (v < 10) v += 0x30;
					else        v += 0x37;
					if ((pos - buf) < (sizeof(buf) - 1)) {
						*pos++ = (char)v;
						len += 1;
					} else {
						*pos = '\0';
						break;
					}
				}
			} else if (*pos == 'M') {
				/* Move the rest of the string further back */
				char *c = buf + len;
				do {
					char *t = c + (8 - 2);
					if ((t - buf) < (sizeof(buf) - 1)) {
						*t = *c;
					} else if ((t - buf) == (sizeof(buf) - 1)) {
						*t = '\0';
						if (*c) len -= 1;
					} else {
						if (*c) len -= 1;
					}
				} while (--c != pos);
				uint8_t mac[8];
				ESP_ERROR_CHECK(esp_read_mac(mac, ESP_MAC_WIFI_SOFTAP));
				--pos;
				/* Write MAC in (almost) BASE64 into string */
				for (int i = 0; i < 8; ++i) {
					uint8_t v = ((mac[(i * 6) / 8] | (mac[(i * 6) / 8 + 1] << 8)) >> ((i * 6) & 0x7)) & 0x3F;
					static const char enc[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789()";
					v = (uint8_t)enc[v];
					if ((pos - buf) < (sizeof(buf) - 1)) {
						*pos++ = (char)v;
						len += 1;
					} else {
						*pos = '\0';
						break;
					}
				}
			}
		} else {
			++pos;
		}
	}
	buf[32] = '\0';
	if (out) {
		strncpy(out, buf, max_len);
	}
	return strlen(buf);
}

esp_err_t wifi_init(void)
{
    ESP_LOGI(TAG, "executing %s() on CORE%i", __func__, xPortGetCoreID());

	/*
	nvs_handle_t nvs_handle;
	esp_err_t r = nvs_open("wifi", NVS_READWRITE, &nvs_handle);
	*/

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    esp_netif_t *ap_netif = esp_netif_create_default_wifi_ap();
	if (!ap_netif) {
		ESP_LOGE(TAGap, "esp_netif_create_default_wifi_ap() returned NULL");
		return ESP_FAIL;
	}
    esp_netif_ip_info_t ip_info;
    IP4_ADDR(&ip_info.ip, 172,30,245,1);
	IP4_ADDR(&ip_info.gw, 172,30,245,1);
	IP4_ADDR(&ip_info.netmask, 255,255,255,0);
	esp_netif_dhcps_stop(ap_netif);
	esp_netif_set_ip_info(ap_netif, &ip_info);
	esp_netif_dhcps_start(ap_netif);

    esp_netif_t *st_netif = esp_netif_create_default_wifi_sta();
	if (!st_netif) {
		ESP_LOGE(TAGWst, "esp_netif_create_default_wifi_sta() returned NULL");
		return ESP_FAIL;
	}

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL, NULL));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL, NULL));

	wifi_cfg_ap.ap.ssid_len = process_SSID(CONFIG_AP_WIFI_SSID, (char *)wifi_cfg_ap.ap.ssid, sizeof(wifi_cfg_ap.ap.ssid));

    if (strlen(CONFIG_AP_WIFI_PASSWORD) == 0) {
        wifi_cfg_ap.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_cfg_ap));
	ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_cfg_sta));

    ESP_ERROR_CHECK(esp_wifi_start());

	//ESP_ERROR_CHECK(esp_wifi_scan_start(NULL, false));

    ESP_LOGI(TAGap, "AP config: SSID:%*s password:%s channel:%d",
			wifi_cfg_ap.ap.ssid_len, (char *)wifi_cfg_ap.ap.ssid,
			CONFIG_AP_WIFI_PASSWORD, CONFIG_AP_WIFI_CHANNEL);

    ESP_LOGI(TAG, "%s() finished", __func__);
	return ESP_OK;
}

static esp_err_t wifi_uri_wifiws_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGws, "executing %s() on CORE%i", __func__, xPortGetCoreID());

	uint8_t buf[128] = { 0 };
	httpd_ws_frame_t ws_pkt;
	memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
	ws_pkt.payload = buf;
	ws_pkt.type = HTTPD_WS_TYPE_TEXT;
	esp_err_t ret = httpd_ws_recv_frame(req, &ws_pkt, 128);
	if (ret != ESP_OK) {
		ESP_LOGE(TAGws, "httpd_ws_recv_frame failed with %d", ret);
		return ret;
	}
	ESP_LOGI(TAGws, "Got packet with message: %s", ws_pkt.payload);
	ESP_LOGI(TAGws, "Packet type: %d", ws_pkt.type);

	if (ws_pkt.type == HTTPD_WS_TYPE_TEXT && !strcmp((char*)ws_pkt.payload, "scan")) {
		bool there = false;
		for(size_t idx = 0; idx < sizeof(wifi_wsfds)/sizeof(wifi_wsfds[0]); ++idx) {
			if (wifi_wsfds[idx] == httpd_req_to_sockfd(req)) {
				there = true;
				break;
			}
		}

		if (!there) {
			for(size_t idx = 0; idx < sizeof(wifi_wsfds)/sizeof(wifi_wsfds[0]); ++idx) {
				if (wifi_wsfds[idx] == 0) {
					wifi_wsfds[idx] = httpd_req_to_sockfd(req);
					break;
				}
			}
		}

		wifi_scan_start_delayed(500);
	}

	return ret;
}

static const httpd_uri_t wifi_uri_wifiws = {
	.uri = "/wifi.ws",
	.method = HTTP_GET,
	.handler = wifi_uri_wifiws_handler,
	.user_ctx = NULL,
	.is_websocket = true
};

void wifi_register_uris(httpd_handle_t server)
{
	wifi_httpServer = server;
	httpd_register_uri_handler(server, &wifi_uri_wifi);
	httpd_register_uri_handler(server, &wifi_uri_wifi_js);
	httpd_register_uri_handler(server, &wifi_uri_wifiws);
}

void wifi_on_fd_close(httpd_handle_t hd, int sockfd)
{
	ESP_LOGI(TAGws, "executing %s(%i) on CORE%i", __func__, sockfd, xPortGetCoreID());

	if (wifi_prevCloseFunc) (*wifi_prevCloseFunc)(hd, sockfd);
	for(size_t idx = 0; idx < sizeof(wifi_wsfds)/sizeof(wifi_wsfds[0]); ++idx) {
		if (wifi_wsfds[idx] == sockfd) wifi_wsfds[idx] = 0;
	}
}

void wifi_register_handlers(httpd_config_t *cfg)
{
	wifi_prevCloseFunc = cfg->close_fn;
	cfg->close_fn = wifi_on_fd_close;
}

/* vim: set ts=4 sw=4 noet nowrap cin ai: */
