#include <stdint.h>
#include <string.h>
#include "esp_system.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h" /* portTICK_RATE_MS */
#if !defined(portTICK_RATE_MS)
#	define portTICK_RATE_MS portTICK_PERIOD_MS
#endif
#include "freertos/task.h" /* vTaskDelay */

#include "plc.h"
#include "st7540.h"
#include "crc7.h"
#include "crc14.h"

static const char *TAG = "PLC";
int plc_rx_T = -1;
int plc_rx_Tsrc = -1;
float plc_rx_Pwr = -1.0;
float plc_rx_Cur = -1.0;
int plc_rx_Ready = -1;

int plc_tx_Pwr = 0;
int plc_tx_Enable = 0;
enum plc_priority_t plc_tx_Priority = plcPrio_INV;
bool plc_tx_FakePwr = false;

enum plc_mode_t plc_TX_mode = plcMode_NORMAL;
enum plc_mode_t plc_RX_mode = plcMode_NORMAL;
unsigned char plc_TX_boot_data[88] = {0};
bool plc_TX_boot_new_data = false;
int plc_st7540_state = 0;
uint16_t plc_RX_boot_Cmd = 0;
uint16_t plc_RX_boot_Ret = 0;
unsigned int plc_RX_boot_Bit = 0;
struct plc_count_t plc_count = {.tx = 0, .rxok = 0, .part = 0, .none = 0};

void plc_handler();

void plc_handler()
{
	static int ct = 0;
	static unsigned char tx_data[5] = {0};
	static uint32_t* const p_tx_data_u32 = (uint32_t*)tx_data;
	static uint32_t last_tx_data = 0;
	static uint32_t ack_tx_data = 0;
	static uint8_t rx_data[5] = {0};
	static int boot_tx_idx = -1;
	static uint32_t boot_rx_data = 0;
	static uint32_t boot_rx_flags = 0;
	static unsigned char TX_boot_data[88] = {0};
	static uint32_t norm_rx_data = 0;
	static uint32_t norm_rx_flags = 0;

	int rxn = 0;
	uint32_t txd = 0;
	if (plc_TX_mode == plcMode_NORMAL) {
		/* Handle normal mode */
		/* Format: .pppPE0 ....... ....... ccccccc cccccc
		 * p -- Power
		 * P -- Priority
		 * E -- Enable
		 * c -- CRC14
		 */
		int pwr = plc_tx_Pwr;
		if (plc_tx_FakePwr) pwr = 3;
		txd = ((pwr & 0x07) << 3) | ((plc_tx_Priority & 0x1) << 2) | ((plc_tx_Enable & 0x1) << 1);

		for (int i = 0; i < 3; ++i) {
			tx_data[i] = txd & 0x7F;
			txd >>= 7;
		}
		uint16_t crc = crc14_7bit_calc(0x3fff, tx_data, 3);
		tx_data[3] = (crc >> 0) & 0x7f;
		tx_data[4] = (crc >> 7) & 0x7f;
		if (*p_tx_data_u32 == last_tx_data && *p_tx_data_u32 == ack_tx_data && ++ct < 3) return;
		ct = 0;
		/* Receive data */
		rxn = st7540_uart_recv(rx_data, 3);
		st7540_uart_flush();
	} else {
		/* Handle boot mode */
		/* Receive data */
		rxn = st7540_uart_recv(rx_data, 3);
		st7540_uart_flush();
		/* Transmit data */
		if (plc_TX_boot_new_data) {
			memcpy(TX_boot_data, plc_TX_boot_data, sizeof(TX_boot_data));
			plc_TX_boot_new_data = false;
			boot_tx_idx = -1;
		}
		/* Boot mode packet */
		/* Format: diiiii1 ddddddd ddddddd ddddddd cccccc
		 * d -- indexed data
		 * i -- index
		 * c -- CRC7
		 */ 
		/* We can skip all data except first and last parts if zero */
		do {
			++boot_tx_idx;
			int idx = boot_tx_idx * 22;
			txd = 0;
			for (int i = (idx >> 3); i < (((idx + 22) >> 3) + 1); ++i) {
				int o = i * 8 - idx;
				uint32_t db = TX_boot_data[i];
				if (o < 0) db >>= -o;
				else       db <<= o;
				txd |= db;
			}
		} while (boot_tx_idx != 0 && boot_tx_idx != 20 && txd == 0);
		txd <<= 6;
		txd |= (boot_tx_idx << 1) | 1;
		for (int i = 0; i < 4; ++i) {
			tx_data[i] = txd & 0x7F;
			txd >>= 7;
		}
		tx_data[4] = crc7_7bit_calc(0x7f, tx_data, 4);
		if (boot_tx_idx == 20) boot_tx_idx = -1;
	}
	esp_err_t err = (st7540_uart_send((const char *)tx_data, 5));
	if (err == ESP_ERR_NOT_SUPPORTED) {
		/* This is overheated condition of ST7540 */
		plc_st7540_state = 1;
		ESP_LOGE(TAG, "ST7540 overheat detected");
		vTaskDelay(1000 / portTICK_RATE_MS);
	} else {
		plc_st7540_state = 0;
	}
	
	/* Process received data */
	if (rxn == 3 && !crc7_7bit_calc(0x7f, rx_data, 3)) {
		if (rx_data[0] & 0x01) {
			plc_RX_mode = plcMode_BOOT;
			/* Boot mode reply: ddddii1 ddddddd ccccccc
			 * d -- indexed data
			 * i -- index
			 * c -- CRC7
			 */
			uint32_t d = (rx_data[0] >> 3) | ((uint32_t)rx_data[1] << 4);
			int id = (rx_data[0] >> 1) & 0x3;
			if (id == 0) {
				boot_rx_data = 0;
				boot_rx_flags = 0;
			}
			boot_rx_flags |= (1 << id);
			boot_rx_data |= (d << (id * 11));
			if (boot_rx_flags == 0x7 && id == 2) {
				plc_RX_boot_Cmd = (boot_rx_data >> 0) & 0xFFFF;
				plc_RX_boot_Ret = (boot_rx_data >> 16) & 0xFFFF;
				plc_RX_boot_Bit = (d >> 10) & 0x1;
				boot_rx_flags = 0;
				boot_rx_data = 0;
			}
		} else {
			plc_RX_mode = plcMode_NORMAL;
			/* Normal mode reply: ddddii0 ddddddd ccccccc
			 * d -- indexed data
			 * i -- index
			 * c -- CRC7
			 * Data format:
			 * pppp...r pppppppp cccccccc tttttttt .
			 * r -- Ready
			 * p -- PV Power
			 * c -- Inverter current
			 * t -- Temperature
			 */
			uint32_t d = (rx_data[0] >> 3) | ((uint32_t)rx_data[1] << 4);
			int id = (rx_data[0] >> 1) & 0x3;
			if (id == 0) {
				norm_rx_data = 0;
				norm_rx_flags = 0;
			}
			norm_rx_flags |= (1 << id);
			norm_rx_data |= (d << (id * 11));
			if (norm_rx_flags == 0x7 && id == 2) {
				plc_rx_Ready = (norm_rx_data >> 0) & 0x1;
				plc_rx_Pwr = (float)((norm_rx_data >> 4) & 0xFFF) / 4095.0f * 2000.0f;
				plc_rx_Cur = (float)((norm_rx_data >> 16) & 0xFF) / 255.0f * 10.0f;
				plc_rx_T = (((norm_rx_data >> 24) & 0xFF) % 85) * 2;
				plc_rx_Tsrc = ((norm_rx_data >> 24) & 0xFF) / 85 + 1;
				norm_rx_flags = 0;
				norm_rx_data = 0;
			}
		}

		ack_tx_data = last_tx_data;
		plc_count.rxok++;
	} else /* rxn != 3 */ {
		plc_rx_T = -1;
		plc_rx_Tsrc = -1;
		plc_rx_Pwr = -1.0f;
		plc_rx_Cur = -1.0f;
		plc_rx_Ready = -1;
		if (rxn) {
			plc_count.part++;
		} else {
			plc_count.none++;
		}
	}
	if (++plc_count.tx == 1001) {
		plc_count.tx = 0;
		plc_count.rxok = 0;
		plc_count.part = 0;
		plc_count.none = 0;
	}

	last_tx_data = *p_tx_data_u32;
}

