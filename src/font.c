#include "font.h"

uint32_t font_find_char(const struct font * font, unsigned int c)
{
	uint32_t chbt = ((font->bbx.w + 7) >> 3) * font->bbx.h;
	const unsigned char * fdata = (const unsigned char *)font->data;
	uint32_t coffset = 0;
	for (;;) {
		uint32_t offset = 0;
		uint32_t rstart = *((uint32_t *)(fdata + offset));
		uint32_t rlen = (uint32_t)(*((uint16_t *)(fdata + offset + 4)));
		do {
			if (c >= rstart && c < (rstart + rlen)) {
				coffset = offset + 6 + chbt * (c - rstart);
				break;
			}
			offset += ((6 + rlen * chbt + 3) >> 2) << 2;
			rstart = *((uint32_t *)(fdata + offset));
			rlen = (uint32_t)(*((uint16_t *)(fdata + offset + 4)));
		} while (rstart || rlen);
		if (coffset || c == font->default_char) break;
		c = font->default_char;
	}
	return coffset;
}

