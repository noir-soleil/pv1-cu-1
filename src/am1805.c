#include "am1805.h"
#include <stdio.h>
#include "sdkconfig.h"
#include "esp_log.h"
#include "driver/i2c.h"
#if !defined(portTICK_RATE_MS)
#	define portTICK_RATE_MS portTICK_PERIOD_MS
#endif

static const char *TAGT = "AM1805";

#define _I2C_NUMBER(num) I2C_NUM_##num
#define I2C_NUMBER(num) _I2C_NUMBER(num)

#define AM1805_ADDR 0xD2

#define AM1805_ENABLED       CONFIG_AM1805_ENABLED
#define AM1805_I2C_PORT_NUM  CONFIG_AM1805_I2C_PORT_NUM
#define AM1805_I2C_SDA       CONFIG_AM1805_I2C_SDA
#define AM1805_I2C_SCL       CONFIG_AM1805_I2C_SCL
#define AM1805_I2C_FREQUENCY CONFIG_AM1805_I2C_FREQUENCY


#define WRITE_BIT I2C_MASTER_WRITE              /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ                /*!< I2C master read */
#define ACK_CHECK_EN 0x1                        /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0                       /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                             /*!< I2C ack value */
#define NACK_VAL 0x1                            /*!< I2C nack value */

#if AM1805_ENABLED
/**
 * @brief i2c master initialization
 */
static esp_err_t am1805_i2c_init(void)
{
    ESP_LOGI(TAGT, "executing %s() on CORE%i", __func__, xPortGetCoreID());
    int i2c_master_port = AM1805_I2C_PORT_NUM;
    i2c_config_t conf = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = AM1805_I2C_SDA,
        .sda_pullup_en = GPIO_PULLUP_DISABLE,
        .scl_io_num = AM1805_I2C_SCL,
        .scl_pullup_en = GPIO_PULLUP_DISABLE,
        .master.clk_speed = AM1805_I2C_FREQUENCY,
        .clk_flags = 0,          /*!< Optional, you can use I2C_SCLK_SRC_FLAG_* flags to choose i2c source clock here. */
    };
    esp_err_t err = i2c_param_config(i2c_master_port, &conf);
    if (err != ESP_OK) {
        return err;
    }
    return i2c_driver_install(i2c_master_port, conf.mode, 0, 0, 0);
}
#endif /* AM1805_ENABLED */

esp_err_t am1805_init()
{
#if AM1805_ENABLED
	esp_err_t ret;
	i2c_cmd_handle_t cmd;

    ESP_LOGI(TAGT, "executing %s() on CORE%i", __func__, xPortGetCoreID());

	ret = am1805_i2c_init();
	if (ret != ESP_OK) {
		ESP_LOGE(TAGT, "am1805_i2c_init returned %s", esp_err_to_name(ret));
		return ret;
	}

	/* Configuration:
	 * 0x1D (Oscillator Status Register) = 0x03
	 * 0x10 (Control1) = 0x33
	 * 0x11 (Control2) = 0x3D
	 * 0x12 (Interrupt Mask) = 0xE0 (Reset value)
	 * 0x13 (SQW) = 0x26 (Reset value)
	 */


	cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, AM1805_ADDR | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x1D, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x03, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, AM1805_ADDR | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x10, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x33, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x3D, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0xE0, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x26, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, AM1805_ADDR | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x1F, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x00, ACK_CHECK_EN);
	i2c_master_stop(cmd);

	ret = i2c_master_cmd_begin(CONFIG_AM1805_I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK) {
		ESP_LOGE(TAGT, "config cmd returned %s", esp_err_to_name(ret));
		return ret;
	}

	cmd = i2c_cmd_link_create();
	uint8_t data_buf[0x31];
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, AM1805_ADDR | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, 0x00, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, AM1805_ADDR | READ_BIT, ACK_CHECK_EN);
	i2c_master_read(cmd, data_buf, sizeof(data_buf) - 1, ACK_VAL);
	i2c_master_read_byte(cmd, &data_buf[sizeof(data_buf) - 1], NACK_VAL);
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(CONFIG_AM1805_I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK) {
		ESP_LOGE(TAGT, "read back cmd returned %s", esp_err_to_name(ret));
		return ret;
	}

	puts("Data from AM1805:");
	printf("Date: %02x.%02x.%02x(%01x) %02x:%02x:%02x.%02x\n",
			(int)(data_buf[4] & 0x3F), 
			(int)(data_buf[5] & 0x1F), 
			(int)data_buf[6],
			(int)(data_buf[7] & 0x07), 
			(int)(data_buf[3] & 0x3F), 
			(int)(data_buf[2] & 0x7F), 
			(int)(data_buf[1] & 0x7F), 
			(int)(data_buf[0] & 0xFF)
		  );
	printf("Alarm: %02x.%02x.xx(%01x) %02x:%02x:%02x.%02x\n",
			(int)(data_buf[12] & 0x3F), 
			(int)(data_buf[13] & 0x1F), 
			(int)(data_buf[14] & 0x07), 
			(int)(data_buf[11] & 0x3F), 
			(int)(data_buf[10] & 0x7F), 
			(int)(data_buf[9] & 0x7F), 
			(int)(data_buf[8] & 0xFF)
		  );

	for (int i = 0x0F; i < sizeof(data_buf); i++) {
		printf("%02x ", (int)data_buf[i]);
		if (i % 8 == 7) puts("");
	}
	puts("\n");

	return ret;
#else
	return ESP_OK;
#endif /* AM1805_ENABLED */
}

esp_err_t am1805_set_bit(uint8_t address, int num, int value)
{
    ESP_LOGI(TAGT, "executing %s() on CORE%i", __func__, xPortGetCoreID());
#if AM1805_ENABLED

	esp_err_t ret;
	i2c_cmd_handle_t cmd;
	uint8_t reg;

	/* Read Control1 register first, then write new value */

	cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, AM1805_ADDR | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, address, ACK_CHECK_EN);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, AM1805_ADDR | READ_BIT, ACK_CHECK_EN);
	i2c_master_read_byte(cmd, &reg, NACK_VAL);
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(CONFIG_AM1805_I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK) {
		ESP_LOGE(TAGT, "read cmd returned %s", esp_err_to_name(ret));
		return ret;
	}

	if (value) {
		reg |= (1 << num);
	} else {
		reg &= ~(1 << num);
	}
	cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, AM1805_ADDR | WRITE_BIT, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, address, ACK_CHECK_EN);
	i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(CONFIG_AM1805_I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);
	if (ret != ESP_OK) {
		ESP_LOGE(TAGT, "write cmd returned %s", esp_err_to_name(ret));
		return ret;
	}

	return ret;
#else 
	return ESP_OK;
#endif /* AM1805_ENABLED */
}

esp_err_t am1805_set_plc_reg_data(int value)
{
	return am1805_set_bit(0x10, 4, value);
}

esp_err_t am1805_set_plc_uart_spi(int value)
{
	return am1805_set_bit(0x10, 5, value);
}

/* vim: set ts=4 sw=4 noet nowrap: */
