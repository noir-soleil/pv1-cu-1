#include "hardware.h"

#include "sdkconfig.h"
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
//#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include <sys/param.h>
#include "esp_netif.h"
#include <esp_http_server.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"

#include "wifi.h"
#include "ota.h"
#include "display.h"
#include "adc.h"
#include "am1805.h"
#include "st7540.h"
#include "plc.h"

#include "ter-u12b.h"

static const char *TAGH = "http";


#if defined(CONFIG_RELAY_GPIO_NUM)
#	define PIN_RELAY CONFIG_RELAY_GPIO_NUM
#elif defined(CONFIG_LED_GPIO_NUM)
#	define PIN_RELAY CONFIG_LED_GPIO_NUM
#endif

#if CONFIG_HAVE_PWRSW
#	define PIN_SEL_ADDR0 CONFIG_PWRSW_A0_GPIO_NUM
#	define PIN_SEL_ADDR1 CONFIG_PWRSW_A1_GPIO_NUM
#endif

//#define GPIO_LED    5

static esp_err_t root_get_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGH, "executing %s() on CORE%i", __func__, xPortGetCoreID());
	extern const char start_index_html[] asm("_binary_index_html_start");
	extern const char end_index_html[] asm("_binary_index_html_end");
	httpd_resp_send(req, start_index_html, end_index_html - start_index_html - 1 /* omit '\0' */);
	return ESP_OK;
}

static const httpd_uri_t uri_root = {
    .uri       = "/",
    .method    = HTTP_GET,
    .handler   = root_get_handler,
    .user_ctx  = NULL
};

static esp_err_t uri_index_js_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGH, "executing %s() on CORE%i", __func__, xPortGetCoreID());
	extern const char start_index_js[] asm("_binary_index_js_start");
	extern const char end_index_js[] asm("_binary_index_js_end");
	httpd_resp_set_type(req, "text/javascript");
	httpd_resp_send(req, start_index_js, end_index_js - start_index_js - 1 /* omit '\0' */);
	return ESP_OK;
}

static const httpd_uri_t uri_index_js = {
	.uri = "/index.js",
	.method = HTTP_GET,
	.handler = uri_index_js_handler,
	.user_ctx = NULL
};

static esp_err_t style_css_get_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGH, "executing %s() on CORE%i", __func__, xPortGetCoreID());
	extern const char start_style_css[] asm("_binary_style_css_start");
	extern const char end_style_css[] asm("_binary_style_css_end");
	httpd_resp_set_type(req, "text/css");
	httpd_resp_send(req, start_style_css, end_style_css - start_style_css - 1 /* omit '\0' */);
	return ESP_OK;
}

static const httpd_uri_t uri_style_css = {
    .uri       = "/style.css",
    .method    = HTTP_GET,
    .handler   = style_css_get_handler,
    .user_ctx  = NULL
};

static esp_err_t ctrl_get_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGH, "executing %s() on CORE%i", __func__, xPortGetCoreID());
    char*  buf;
    size_t buf_len;

    /* Read URL query string length and allocate memory for length + 1,
     * extra byte for null termination */
    buf_len = httpd_req_get_url_query_len(req) + 1;
    if (buf_len > 1) {
        buf = malloc(buf_len);
        if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAGH, "Found URL query => %s", buf);
            char param[32];
            /* Get value of expected key from query string */
            if (httpd_query_key_value(buf, "relay", param, sizeof(param)) == ESP_OK && !strcmp(param, "ON")) {
#if defined(PIN_RELAY)
				gpio_set_level(PIN_RELAY, 1);
#endif
            } else if (httpd_query_key_value(buf, "relay", param, sizeof(param)) == ESP_OK) {
#if defined(PIN_RELAY)
				gpio_set_level(PIN_RELAY, 0);
#endif
            }
            if (httpd_query_key_value(buf, "mosfet", param, sizeof(param)) == ESP_OK && !strcmp(param, "ON")) {
#if CONFIG_HAVE_PWRSW
				gpio_set_level(PIN_SEL_ADDR0, 1);
#endif
            } else if (httpd_query_key_value(buf, "mosfet", param, sizeof(param)) == ESP_OK) {
#if CONFIG_HAVE_PWRSW
				gpio_set_level(PIN_SEL_ADDR0, 0);
#endif
            }
            if (httpd_query_key_value(buf, "pu", param, sizeof(param)) == ESP_OK && !strcmp(param, "ON")) {
				plc_tx_Enable = 1;
            } else if (httpd_query_key_value(buf, "pu", param, sizeof(param)) == ESP_OK) {
				plc_tx_Enable = 0;
            }
            if (httpd_query_key_value(buf, "prio", param, sizeof(param)) == ESP_OK && !strcmp(param, "INV")) {
				plc_tx_Priority = plcPrio_INV;
            } else if (httpd_query_key_value(buf, "prio", param, sizeof(param)) == ESP_OK) {
				plc_tx_Priority = plcPrio_HEATER;
            }
            if (httpd_query_key_value(buf, "fp", param, sizeof(param)) == ESP_OK && !strcmp(param, "ON")) {
				plc_tx_FakePwr = true;
            } else if (httpd_query_key_value(buf, "fp", param, sizeof(param)) == ESP_OK) {
				plc_tx_FakePwr = false;
            }
        }
        free(buf);
    }

    /* Send response with custom headers and body set as the
     * string passed in user context*/
	static char result_str[64];
	int relay =
#if defined(PIN_RELAY)
		gpio_get_level(PIN_RELAY)
#else
		0
#endif
		;
	int mosfet =
#if CONFIG_HAVE_PWRSW
		gpio_get_level(PIN_SEL_ADDR0)
#else
		0
#endif
		;
	snprintf(result_str, sizeof(result_str),
			"%s\n"
			"%s\n"
			"%s\n"
			"%s\n"
			"%s\n",
			(relay ? "<b>ON</b>" : "OFF"),
			(mosfet ? "<b>ON</b>" : "OFF"),
			(plc_tx_Enable ? "<b>ON</b>" : "OFF"),
			((plc_tx_Priority == plcPrio_INV) ? "INV" : "HTR"),
			(plc_tx_FakePwr ? "<b>ON</b>" : "OFF")
			);
    httpd_resp_send(req, result_str, HTTPD_RESP_USE_STRLEN);

    return ESP_OK;
}

static const httpd_uri_t uri_ctrl = {
    .uri       = "/ctrl",
    .method    = HTTP_GET,
    .handler   = ctrl_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};

static esp_err_t stat_get_handler(httpd_req_t *req)
{
    //printf("executing %s() on CORE%i\n", __func__, xPortGetCoreID());
	static char buf[256];

	static char tmax_buf[16];
	snprintf(tmax_buf, sizeof(tmax_buf), "%i°C @%i", plc_rx_T, plc_rx_Tsrc);

	static char p_buf[16];
	snprintf(p_buf, sizeof(p_buf), "%.1fW", plc_rx_Pwr);

	static char c_buf[16];
	snprintf(c_buf, sizeof(c_buf), "%.2fA", plc_rx_Cur);

	float ct_tx = (plc_count.tx) < 1 ? 1.0f : (float)plc_count.tx;
	snprintf(buf, sizeof(buf),
			"%.3fW\n"
			"%s\n"
			"%s\n"
			"%s\n"
			"%s\n"
			"%.1f%%\n"
			"removed\n"
			"%.1f%%\n"
			"removed\n"
			"%s\n",
			adc_get_pwr(),
			((plc_rx_T >= 0) ? tmax_buf : "n/a"),
			((plc_rx_Pwr >= 0.0f) ? p_buf : "n/a"),
			((plc_rx_Cur >= 0.0f) ? c_buf : "n/a"),
			((plc_rx_Ready == 1) ? "Ready" : ((plc_rx_Ready == 0) ? "Wait" : "n/a")),
			((float)plc_count.part + (float)(plc_count.none)) / ct_tx * 100.0f,
			//plc_max_noack_seq,
			((float)(plc_count.none)) / ct_tx * 100.0f,
			//plc_max_norep_seq,
			(plc_st7540_state ? "OverTemp" : "OK")
			);

    httpd_resp_send(req, buf, HTTPD_RESP_USE_STRLEN);

    return ESP_OK;
}

static const httpd_uri_t uri_stat = {
    .uri       = "/stat",
    .method    = HTTP_GET,
    .handler   = stat_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};

static esp_err_t sense_get_handler(httpd_req_t *req)
{
	float * fbuf = adc_get_fbuf(0);
	if (!fbuf) {
		httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Unable to get adc fbuf");
	} else {
		size_t sz = adc_get_fbuf_size();

		static char buf[1024];
		size_t idx = 0;
		size_t i;

		for (i = 0; i < sz / 4; ++i) {
			idx += snprintf(&buf[idx], sizeof(buf) - idx, "%.3f,%.2f,%.3f\n", (float)i * 0.25f, fbuf[i * 4], fbuf[i * 4 + 1]);
			if ((sizeof(buf) - idx) < 30) {
				httpd_resp_send_chunk(req, buf, idx);
				idx = 0;
			}
		}
		if (idx > 0) {
			httpd_resp_send_chunk(req, buf, idx);
		}
		httpd_resp_send_chunk(req, buf, 0);

		adc_give_fbuf(0);
	}
    return ESP_OK;
}

static const httpd_uri_t uri_sense = {
    .uri       = "/sense",
    .method    = HTTP_GET,
    .handler   = sense_get_handler,
    .user_ctx  = NULL
};


/* This handler allows the custom error handling functionality to be
 * tested from client side. For that, when a PUT request 0 is sent to
 * URI /ctrl, the /hello and /echo URIs are unregistered and following
 * custom error handler http_404_error_handler() is registered.
 * Afterwards, when /hello or /echo is requested, this custom error
 * handler is invoked which, after sending an error message to client,
 * either closes the underlying socket (when requested URI is /echo)
 * or keeps it open (when requested URI is /hello). This allows the
 * client to infer if the custom error handler is functioning as expected
 * by observing the socket state.
 */
esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    printf("executing %s() on CORE%i\n", __func__, xPortGetCoreID());
#if 0
    if (strcmp("/hello", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/hello URI is not available");
        /* Return ESP_OK to keep underlying socket open */
        return ESP_OK;
    } else if (strcmp("/echo", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/echo URI is not available");
        /* Return ESP_FAIL to close underlying socket */
        return ESP_FAIL;
    }
#endif
    /* For any other URI send 404 and close socket */
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Not found");
    return ESP_FAIL;
}

#if 0
/* An HTTP PUT handler. This demonstrates realtime
 * registration and deregistration of URI handlers
 */
static esp_err_t ctrl_put_handler(httpd_req_t *req)
{
    printf("executing %s() on CORE%i\n", __func__, xPortGetCoreID());
    char buf;
    int ret;

    if ((ret = httpd_req_recv(req, &buf, 1)) <= 0) {
        if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
            httpd_resp_send_408(req);
        }
        return ESP_FAIL;
    }

    if (buf == '0') {
        /* URI handlers can be unregistered using the uri string */
        ESP_LOGI(TAGH, "Unregistering /hello and /echo URIs");
        httpd_unregister_uri(req->handle, "/hello");
        httpd_unregister_uri(req->handle, "/echo");
        /* Register the custom error handler */
        httpd_register_err_handler(req->handle, HTTPD_404_NOT_FOUND, http_404_error_handler);
    }
    else {
        ESP_LOGI(TAGH, "Registering /hello and /echo URIs");
        httpd_register_uri_handler(req->handle, &hello);
        httpd_register_uri_handler(req->handle, &echo);
        /* Unregister custom error handler */
        httpd_register_err_handler(req->handle, HTTPD_404_NOT_FOUND, NULL);
    }

    /* Respond with empty body */
    httpd_resp_send(req, NULL, 0);
    return ESP_OK;
}

static const httpd_uri_t ctrl = {
    .uri       = "/ctrl_old",
    .method    = HTTP_PUT,
    .handler   = ctrl_put_handler,
    .user_ctx  = NULL
};

#endif

static httpd_handle_t start_webserver(void)
{
    printf("executing %s() on CORE%i\n", __func__, xPortGetCoreID());
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;
	config.core_id = xPortGetCoreID();
	config.max_uri_handlers = 16;

	ota_register_handlers(&config);
	wifi_register_handlers(&config);

    // Start the httpd server
    ESP_LOGI(TAGH, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAGH, "Registering URI handlers");
        httpd_register_uri_handler(server, &uri_root);
        httpd_register_uri_handler(server, &uri_index_js);
        httpd_register_uri_handler(server, &uri_ctrl);
        httpd_register_uri_handler(server, &uri_stat);
        httpd_register_uri_handler(server, &uri_sense);
        httpd_register_uri_handler(server, &uri_style_css);
		ota_register_uris(server);
		wifi_register_uris(server);
        return server;
    }

    ESP_LOGE(TAGH, "Error starting server!");
    return NULL;
}

static void stop_webserver(httpd_handle_t server)
{
    printf("executing %s() on CORE%i\n", __func__, xPortGetCoreID());
    // Stop the httpd server
    httpd_stop(server);
}

#if 0
static void disconnect_handler(void* arg, esp_event_base_t event_base,
                               int32_t event_id, void* event_data)
{
    httpd_handle_t* server = (httpd_handle_t*) arg;
    if (*server) {
        ESP_LOGI(TAGH, "Stopping webserver");
        stop_webserver(*server);
        *server = NULL;
    }
}

static void connect_handler(void* arg, esp_event_base_t event_base,
                            int32_t event_id, void* event_data)
{
    httpd_handle_t* server = (httpd_handle_t*) arg;
    if (*server == NULL) {
        ESP_LOGI(TAGH, "Starting webserver");
        *server = start_webserver();
    }
}
#endif


void app_main(void)
{
	static TaskHandle_t ADCtask_hndl;

    printf("executing %s() on CORE%i\n", __func__, xPortGetCoreID());
#if BOARD_TTGO
	gpio_set_direction(POWER_ON_GPIO, GPIO_MODE_OUTPUT);
	gpio_set_level(POWER_ON_GPIO, 0);
#endif /* BOARD_TTGO */

    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_INPUT_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
	io_conf.pin_bit_mask = 0
#if defined(PIN_RELAY)
		| (1ULL << PIN_RELAY)
#endif
#if CONFIG_HAVE_PWRSW
		| (1ULL << PIN_SEL_ADDR0) | (1ULL << PIN_SEL_ADDR1)
#endif
#if CONFIG_BOARD_TTGO
		| (1ULL << 18) | (1ULL << 19)
#endif
		;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

	/*
    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = ((1ULL<<GPIO_LED));
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);
    */


    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

	am1805_init();
	st7540_init();
	xTaskCreatePinnedToCore(&adc_task, "ADCtask", 2048, NULL, tskIDLE_PRIORITY + 1, &ADCtask_hndl, 1);

	// FIXME: enable relay
	//gpio_set_level(PIN_RELAY, 1);
	vTaskDelay(1000 / portTICK_RATE_MS);

	display_init();

    ESP_ERROR_CHECK(wifi_init());

    static httpd_handle_t server = NULL;
    /* Start the server */
    server = start_webserver();

    //ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &connect_handler, &server));
    //ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_AP_START, &connect_handler, &server));

    //ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &disconnect_handler, &server));
    //ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_AP_STOP, &disconnect_handler, &server));

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());
	const wifi_ap_config_t *ap_cfg = wifi_get_ap_config();
	char str[22];
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-truncation"
	(void)snprintf(str, sizeof(str), "AP: %*s", (int)ap_cfg->ssid_len, (char *)ap_cfg->ssid);
	display_draw_string(str, &font_ter_u12b_bdf, 0, 0, 0);
	(void)snprintf(str, sizeof(str), "PS: %s", (char *)ap_cfg->password);
	display_draw_string(str, &font_ter_u12b_bdf, 0, 12, 0);
#pragma GCC diagnostic pop

    int cnt = 0;

	/*
    while(1) {
        printf("cnt: %d, pwr: %.3f\n", cnt++, adc_get_pwr());
        vTaskDelay(1000 / portTICK_RATE_MS);
		oled_set_inversion(cnt % 2);
		if (cnt == 30) break;
    }
	adc_restart();
	cnt = 0;

	while(!adc_buffer_full()) {
        vTaskDelay(1000 / portTICK_RATE_MS);
		fputs(".", stdout);
		oled_set_inversion(cnt % 2);
	}

	const int32_t *v_buf = adc_get_v_buffer();
	const int32_t *c_buf = adc_get_c_buffer();
	uint32_t sz = adc_buffer_size();
	for (uint32_t i = 0; i < sz; ++i) {
        printf("%i\t%i\n", v_buf[i], c_buf[i]);
	}
	*/
	int disp_inv = 0;
	bool sta = false;
    while(1) {
        //printf("cnt: %d, pwr: %.3f\n", cnt++, adc_get_pwr());
        vTaskDelay(500 / portTICK_RATE_MS);
		if (wifi_sta_ip4.addr != 0 && !sta) {
			const wifi_sta_config_t *sta_cfg = wifi_get_sta_config();
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-truncation"
			(void)snprintf(str, sizeof(str), "ST: %s", (char *)sta_cfg->ssid);
			display_draw_string(str, &font_ter_u12b_bdf, 0, 24, 0);
#pragma GCC diagnostic pop
			(void)snprintf(str, sizeof(str), "IP: " IPSTR, IP2STR(&wifi_sta_ip4));
			display_draw_string(str, &font_ter_u12b_bdf, 0, 36, 0);
			sta = true;
		} else if (wifi_sta_ip4.addr == 0 && sta) {
			display_fill_area(0, 24, 128, 24, 0);
			sta = false;
		}
		display_fill_area(125, 61, 3, 3, (disp_inv ^= 1));
    }
}


#if 0
/* GPIO Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"

/**
 * Brief:
 * This test code shows how to configure gpio and how to use gpio interrupt.
 *
 * GPIO status:
 * GPIO5: output
 *
 * Test:
 * Blink with GPIO5
 *
 */

/*
static xQueueHandle gpio_evt_queue = NULL;

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;
    xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

static void gpio_task_example(void* arg)
{
    uint32_t io_num;
    for(;;) {
        if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
            printf("GPIO[%d] intr, val: %d\n", io_num, gpio_get_level(io_num));
        }
    }
}
*/

void app_main(void)
{
#if 0
    //interrupt of rising edge
    io_conf.intr_type = GPIO_INTR_POSEDGE;
    //bit mask of the pins, use GPIO4/5 here
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    //set as input mode
    io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);

    //change gpio intrrupt type for one pin
    gpio_set_intr_type(GPIO_INPUT_IO_0, GPIO_INTR_ANYEDGE);

    //create a queue to handle gpio event from isr
    gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    //start gpio task
    xTaskCreate(gpio_task_example, "gpio_task_example", 2048, NULL, 10, NULL);

    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void*) GPIO_INPUT_IO_0);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_IO_1, gpio_isr_handler, (void*) GPIO_INPUT_IO_1);

    //remove isr handler for gpio number.
    gpio_isr_handler_remove(GPIO_INPUT_IO_0);
    //hook isr handler for specific gpio pin again
    gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void*) GPIO_INPUT_IO_0);
#endif

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());
    printf("%s() is on CORE%i\n", __func__, xPortGetCoreID());

    int cnt = 0;
    while(1) {
        printf("cnt: %d\n", cnt++);
        vTaskDelay(1000 / portTICK_RATE_MS);
        gpio_set_level(GPIO_LED, cnt % 2);
    }
}
#endif

/* vim: set ts=4 sw=4 noet nowrap: */
