/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _INCLUDED_CRC7_H_
#define _INCLUDED_CRC7_H_

#include <string.h>

/* POLY: (0x5b; 0xb7), len: 7, data: 7bit, init: 0x7f, RefIn: Y, RefOut: Y, XorOut: 0x00 */

#define CRC7_7BIT_INIT 0x7F

extern unsigned char crc7_7bit_table[128];

static inline unsigned char crc7_7bit_byte(unsigned char crc, unsigned char data)
{
	return crc7_7bit_table[(crc ^ data) & 0x7F];
}

static inline unsigned char crc7_7bit_calc(unsigned char crc, const void *data, size_t len)
{
	const unsigned char *m = (const unsigned char*)data;
	while (len--) crc = crc7_7bit_byte(crc, *m++);
	return crc;
}

#endif /* ndef _INCLUDED_CRC7_H_ */
