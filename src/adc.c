#include "adc.h"
#include "sdkconfig.h"

#define HAVE_ADC CONFIG_HAVE_ADC
#define ADC_MOSI_GPIO_NUM CONFIG_ADC_MOSI_GPIO_NUM
#define ADC_MISO_GPIO_NUM CONFIG_ADC_MISO_GPIO_NUM
#define ADC_CLK_GPIO_NUM CONFIG_ADC_CLK_GPIO_NUM
#define ADC_DREADY_GPIO_NUM CONFIG_ADC_DREADY_GPIO_NUM

#define REPLACE_SPI_FUNCTIONS 1

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "esp_system.h"
#include "esp_log.h"
//#include "esp_event.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "esp_timer.h"
#include "esp_rom_crc.h"
#include "freertos/semphr.h"

#if !(HAVE_ADC)
#	if (ESP_IDF_VERSION_MAJOR == 5)
#		include "driver/gptimer.h"
#	else
#		include "driver/timer.h"
#	endif
#endif

#if REPLACE_SPI_FUNCTIONS
#	include <stdatomic.h>
#	include "driver/spi_common_internal.h"
#	include "hal/spi_hal.h"
#	include "esp_heap_caps.h"
#endif

#define PLC_MASTER

#include "st7540.h"
#include "crc7.h"
#include "crc14.h"
#include "plc.h"

/* ADC definitions */
#define ADC_HOST HSPI_HOST

#define ADC_REG_ID 0x00
#define ADC_REG_STATUS 0x01
#define ADC_REG_MODE 0x02
#define ADC_REG_CLOCK 0x03
#define ADC_REG_GAIN 0x04
#define ADC_REG_CFG 0x06
#define ADC_REG_THRSHLD_MSB 0x07
#define ADC_REG_THRSHLD_LSB 0x08
#define ADC_REG_CH0_CFG 0x09
#define ADC_REG_CH0_OCAL_MSB 0x0A
#define ADC_REG_CH0_OCAL_LSB 0x0B
#define ADC_REG_CH0_GCAL_MSB 0x0C
#define ADC_REG_CH0_GCAL_LSB 0x0D
#define ADC_REG_CH1_CFG 0x0E
#define ADC_REG_CH1_OCAL_MSB 0x0F
#define ADC_REG_CH1_OCAL_LSB 0x10
#define ADC_REG_CH1_GCAL_MSB 0x11
#define ADC_REG_CH1_GCAL_LSB 0x12
#define ADC_REG_CH2_CFG 0x13
#define ADC_REG_CH2_OCAL_MSB 0x14
#define ADC_REG_CH2_OCAL_LSB 0x15
#define ADC_REG_CH2_GCAL_MSB 0x16
#define ADC_REG_CH2_GCAL_LSB 0x17
#define ADC_REG_CH3_CFG 0x18
#define ADC_REG_CH3_OCAL_MSB 0x19
#define ADC_REG_CH3_OCAL_LSB 0x1A
#define ADC_REG_CH3_GCAL_MSB 0x1B
#define ADC_REG_CH3_GCAL_LSB 0x1C
#define ADC_REG_REGMAP_CRC 0x3E
#define ADC_REG_RESERVED 0x3F

#define H_BYTE(v) (((v) >> 8) & 0x0FF)
#define L_BYTE(v) (((v) >> 0) & 0x0FF)

#define ADC_CMD_NULL 0x0000
#define ADC_CMD_RESET 0x0011
#define ADC_ACK_RESET 0xFF24
#define ADC_CMD_STANDBY 0x0022
#define ADC_CMD_WAKEUP 0x0033
#define ADC_CMD_LOCK 0x0555
#define ADC_CMD_UNLOCK 0x0655
#define ADC_CMD_RREG(a, n) (0xA000 | (((a) & 0x3F) << 7) | ((n) & 0x7F))
#define ADC_ACK_RREG(a, n) (0xE000 | (((a) & 0x3F) << 7) | ((n) & 0x7F))
#define ADC_CMD_WREG(a, n) (0x6000 | (((a) & 0x3F) << 7) | ((n) & 0x7F))
#define ADC_ACK_WREG(a, n) (0x4000 | (((a) & 0x3F) << 7) | ((n) & 0x7F))


static const double adc_V_scale = 1.2 / (double)0x800000 * (510.0 * 3.0 + (3.65 * 300.0) / (3.65 + 300.0)) / ((3.65 * 300.0) / (3.65 + 300.0));
static const double adc_C_scale = 1.2 / (double)0x800000 * 60.0 / 0.4;

/* Graph related */
float adc_fbuf[2][4 * 200];
int adc_fbuf_lock[2];
SemaphoreHandle_t adc_fbuf_Mutex;
StaticSemaphore_t adc_fbuf_StaticSemBuf;

static void adc_fbuf_init()
{
	size_t i, j;
	for (i = 0; i < sizeof(adc_fbuf)/sizeof(adc_fbuf[0]); ++i) {
		for (j = 0; j < sizeof(adc_fbuf[0])/sizeof(adc_fbuf[0][0]); ++j) {
			adc_fbuf[i][j] = 0.1f;
		}
		adc_fbuf_lock[i] = 0;
	}
	adc_fbuf_Mutex = xSemaphoreCreateMutexStatic(&adc_fbuf_StaticSemBuf);
}

size_t adc_get_fbuf_size()
{
	return sizeof(adc_fbuf[0])/sizeof(adc_fbuf[0][0]);
}

float* adc_get_fbuf(int tag)
{
	if (xSemaphoreTake(adc_fbuf_Mutex, portMAX_DELAY) == pdTRUE) {
		float *ret = NULL;
		if (!(adc_fbuf_lock[0] & (1 << tag))) {
			adc_fbuf_lock[0] |= (1 << tag);
			ret = adc_fbuf[0];
		} else if (!(adc_fbuf_lock[1] & (1 << tag))) {
			adc_fbuf_lock[1] |= (1 << tag);
			ret = adc_fbuf[1];
		}
		xSemaphoreGive(adc_fbuf_Mutex);
		return ret;
	}
	return NULL;
}

void adc_give_fbuf(int tag)
{
	if (xSemaphoreTake(adc_fbuf_Mutex, portMAX_DELAY) == pdTRUE) {
		if (adc_fbuf_lock[0] != -1 && (adc_fbuf_lock[0] & (1 << tag))) adc_fbuf_lock[0] &= ~(1 << tag);
		if (adc_fbuf_lock[1] != -1 && (adc_fbuf_lock[1] & (1 << tag))) adc_fbuf_lock[1] &= ~(1 << tag);
		xSemaphoreGive(adc_fbuf_Mutex);
	}
}

static float* adc_try_get_next_fbuf()
{
	static size_t last_idx = 1;
	if (xSemaphoreTake(adc_fbuf_Mutex, (TickType_t)0) == pdTRUE) {
		float *ret = NULL;
		if (adc_fbuf_lock[last_idx] == -1) adc_fbuf_lock[last_idx] = 0;
		size_t new_idx = last_idx ^ 1;
		if (!adc_fbuf_lock[new_idx]) {
			last_idx = new_idx;
			ret = adc_fbuf[last_idx];
			adc_fbuf_lock[last_idx] = -1;
		}
		xSemaphoreGive(adc_fbuf_Mutex);
		return ret;
	}
	return NULL;
}

spi_device_handle_t adc_spi;
int adc_core = -1;
double adc_pwr = 0.0;

float adc_get_pwr()
{
	return adc_pwr;
}

#if REPLACE_SPI_FUNCTIONS
/* From spi_master.c */

typedef struct spi_device_t spi_device_t;
typedef struct {
    spi_transaction_t   *trans;
    const uint32_t *buffer_to_send;   //equals to tx_data, if SPI_TRANS_USE_RXDATA is applied; otherwise if original buffer wasn't in DMA-capable memory, this gets the address of a temporary buffer that is;
                                //otherwise sets to the original buffer or NULL if no buffer is assigned.
    uint32_t *buffer_to_rcv;    // similar to buffer_to_send
} spi_trans_priv_t;

typedef struct {
    int id;
    spi_device_t* device[DEV_NUM_MAX];
    intr_handle_t intr;
    spi_hal_context_t hal;
    spi_trans_priv_t cur_trans_buf;
    int cur_cs;     //current device doing transaction
    const spi_bus_attr_t* bus_attr;

    /**
     * the bus is permanently controlled by a device until `spi_bus_release_bus`` is called. Otherwise
     * the acquiring of SPI bus will be freed when `spi_device_polling_end` is called.
     */
    spi_device_t* device_acquiring_lock;

//debug information
    bool polling;   //in process of a polling, avoid of queue new transactions into ISR
} spi_host_t;

struct spi_device_t {
    int id;
    QueueHandle_t trans_queue;
    QueueHandle_t ret_queue;
    spi_device_interface_config_t cfg;
    spi_hal_dev_config_t hal_dev;
    spi_host_t *host;
    spi_bus_lock_dev_handle_t dev_lock;
};

/* From spi_bus_lock.c */
struct spi_bus_lock_dev_t;
typedef struct spi_bus_lock_dev_t spi_bus_lock_dev_t;

typedef struct spi_bus_lock_t spi_bus_lock_t;


#define MAX_DEV_NUM     10

// Bit 29-20: lock bits, Bit 19-10: pending bits
// Bit 9-0: request bits, Bit 30:
#define LOCK_SHIFT      20
#define PENDING_SHIFT   10
#define REQ_SHIFT       0

#define WEAK_BG_FLAG    BIT(30)    /**< The bus is permanently requested by background operations.
                                     * This flag is weak, will not prevent acquiring of devices. But will help the BG to be re-enabled again after the bus is release.
                                     */

// get the bit mask wher bit [high-1, low] are all 1'b1 s.
#define BIT1_MASK(high, low)   ((UINT32_MAX << (high)) ^ (UINT32_MAX << (low)))

#define LOCK_BIT(mask)      ((mask) << LOCK_SHIFT)
#define REQUEST_BIT(mask)   ((mask) << REQ_SHIFT)
#define PENDING_BIT(mask)   ((mask) << PENDING_SHIFT)
#define DEV_MASK(id)        (LOCK_BIT(1<<id) | PENDING_BIT(1<<id) | REQUEST_BIT(1<<id))
#define ID_DEV_MASK(mask)   (ffs(mask) - 1)

#define REQ_MASK            BIT1_MASK(REQ_SHIFT+MAX_DEV_NUM, REQ_SHIFT)
#define PEND_MASK           BIT1_MASK(PENDING_SHIFT+MAX_DEV_NUM, PENDING_SHIFT)
#define BG_MASK             BIT1_MASK(REQ_SHIFT+MAX_DEV_NUM*2, REQ_SHIFT)
#define LOCK_MASK           BIT1_MASK(LOCK_SHIFT+MAX_DEV_NUM, LOCK_SHIFT)

#define DEV_REQ_MASK(dev)   ((dev)->mask & REQ_MASK)
#define DEV_PEND_MASK(dev)  ((dev)->mask & PEND_MASK)
#define DEV_BG_MASK(dev)    ((dev)->mask & BG_MASK)

struct spi_bus_lock_t {
    /**
     * The core of the lock. These bits are status of the lock, which should be always available.
     * No intermediate status is allowed. This is realized by atomic operations, mainly
     * `atomic_fetch_and`, `atomic_fetch_or`, which atomically read the status, and bitwise write
     * status value ORed / ANDed with given masks.
     *
     * The request bits together pending bits represent the actual bg request state of one device.
     * Either one of them being active indicates the device has pending bg requests.
     *
     * Whenever a bit is written to the status, it means the a device on a task is trying to
     * acquire the lock. But this will succeed only when no LOCK or BG bits active.
     *
     * The acquiring processor is responsible to call the scheduler to pass its role to other tasks
     * or the BG, unless it clear the last bit in the status register.
     */
    //// Critical resources, they are only writable by acquiring processor, and stable only when read by the acquiring processor.
    atomic_uint_fast32_t    status;
    spi_bus_lock_dev_t* volatile acquiring_dev;   ///< The acquiring device
    bool                volatile acq_dev_bg_active;    ///< BG is the acquiring processor serving the acquiring device, used for the wait_bg to skip waiting quickly.
    bool                volatile in_isr;         ///< ISR is touching HW
    //// End of critical resources

    atomic_intptr_t     dev[DEV_NUM_MAX];     ///< Child locks.
    bg_ctrl_func_t      bg_enable;      ///< Function to enable background operations.
    bg_ctrl_func_t      bg_disable;     ///< Function to disable background operations
    void*               bg_arg;            ///< Argument for `bg_enable` and `bg_disable` functions.

    spi_bus_lock_dev_t* last_dev;       ///< Last used device, to decide whether to refresh all registers.
    int                 periph_cs_num;  ///< Number of the CS pins the HW has.

    //debug information
    int                 host_id;        ///< Host ID, for debug information printing
    uint32_t            new_req;        ///< Last int_req when `spi_bus_lock_bg_start` is called. Debug use.
};

struct spi_bus_lock_dev_t {
    SemaphoreHandle_t   semphr;     ///< Binray semaphore to notify the device it claimed the bus
    spi_bus_lock_t*     parent;     ///< Pointer to parent spi_bus_lock_t
    uint32_t            mask;       ///< Bitwise OR-ed mask of the REQ, PEND, LOCK bits of this device
};

esp_err_t IRAM_ATTR spi_device_queue_trans_fromISR(spi_device_handle_t handle, spi_transaction_t *trans_desc, TickType_t ticks_to_wait);
#endif

static const char *TAGA = "spi:ADC";

static int adc_data_ct = -3;
static uint8_t adc_tx_buffer[6 * 4] __attribute__((aligned(4)))
	= {0};
static uint8_t adc_rx_buffer[6 * 4] __attribute__((aligned(4)));
static spi_transaction_t adc_transaction = {0};

int32_t adc_v_bufc[512];
int32_t adc_c_bufc[512];
uint32_t volatile adc_bufc_idx = 0;

int32_t adc_v_buffer[1024];
int32_t adc_c_buffer[1024];

static int adc_50Hz_plc_trig = 0;

const int32_t* adc_get_v_buffer()
{
    printf("executing %s() on CORE%i\n", __func__, xPortGetCoreID());
	printf("adc ISRs on CORE%i\n", adc_core);
	return (const int32_t *)adc_v_buffer;
}

const int32_t* adc_get_c_buffer()
{
	return (const int32_t *)adc_c_buffer;
}

bool adc_buffer_full()
{
	return (adc_data_ct == sizeof(adc_v_buffer) / sizeof(adc_v_buffer[0]));
}

uint32_t adc_buffer_size()
{
	return sizeof(adc_v_buffer) / sizeof(adc_v_buffer[0]);
}

void adc_restart()
{
	adc_data_ct = 0;
}

static void IRAM_ATTR adc_handle_rx_data(const uint8_t *buf)
{
	static int32_t pv = 1;
	static uint32_t ct = 0;
	static uint32_t pct = 0;

	int32_t v = (buf[3] << 16) | (buf[4] << 8) | (buf[5] << 0);
	/* Sign extend */
	if (v & 0x800000) v |= 0xFF000000;
	int32_t c = (buf[6] << 16) | (buf[7] << 8) | (buf[8] << 0);
	/* Sign extend */
	if (c & 0x800000) c |= 0xFF000000;
	++ct;

	uint32_t dt = ct - pct;

	if ((dt > 145 && pv <= 0 && v > 0) || dt > 178) {
		pct = ct;
		adc_50Hz_plc_trig = 1;
	}
	pv = v;

	adc_v_bufc[adc_bufc_idx] = v;
	adc_c_bufc[adc_bufc_idx] = c;
	adc_bufc_idx = (adc_bufc_idx + 1) & (sizeof(adc_v_bufc) / sizeof(adc_v_bufc[0]) - 1);

	if (adc_data_ct < sizeof(adc_v_buffer) / sizeof(adc_v_buffer[0])) {
		adc_v_buffer[adc_data_ct] = v;
		adc_c_buffer[adc_data_ct] = c;
		adc_data_ct++;
	}
}

//This function is called (in irq context!) after SPI transmission ends.
void IRAM_ATTR adc_spi_post_transfer_callback(spi_transaction_t *t)
{
	adc_core = xPortGetCoreID();
	if (adc_data_ct < -1) return;
	if (adc_data_ct < 0) {
		/* Need to shedule second data read when reading for the first time */
		adc_transaction.rxlength = 0;
#if REPLACE_SPI_FUNCTIONS
		esp_err_t ret = spi_device_queue_trans_fromISR(adc_spi, &adc_transaction, portMAX_DELAY);
#else
		esp_err_t ret = spi_device_queue_trans(adc_spi, &adc_transaction, portMAX_DELAY);
#endif
		//ESP_ERROR_CHECK(ret);
		adc_data_ct++;
		return;
	}
	adc_handle_rx_data((const uint8_t *)t->rx_buffer);
}

static void IRAM_ATTR adc_drdy_isr_handler(void* arg)
{
	/* FIXME: should not print from this function */
	adc_transaction.rxlength = 0;
#if REPLACE_SPI_FUNCTIONS
		esp_err_t ret = spi_device_queue_trans_fromISR(adc_spi, &adc_transaction, portMAX_DELAY);
#else
		esp_err_t ret = spi_device_queue_trans(adc_spi, &adc_transaction, portMAX_DELAY);
#endif
    //ESP_ERROR_CHECK(ret);
}

#if !(HAVE_ADC)

uint32_t cp0_regs[18];

static bool adc_emul_handler(void *arg)
{
#if CONFIG_BOARD_TTGO
	gpio_set_level(18, 1);
#endif

	// get FPU state
	uint32_t cp_state = xthal_get_cpenable();

	if(!cp_state) {
		// enable FPU
		xthal_set_cpenable(1);
	}
	xthal_save_cp0(cp0_regs);

	adc_core = xPortGetCoreID();
	static float a = 0.0f;
	float s0 = sinf(a * 2.0f * 3.14159265359);

	if ((a = a + 50.0f / 4000.0f) > 1.0f) a = a - 1.0f;

	int32_t v = (int32_t)(s0 * 310.0f / (float)adc_V_scale);
	int32_t c0 = (int32_t)(s0 * 1.0f / (float)adc_C_scale);

	xthal_restore_cp0(cp0_regs);
	if(!cp_state) {
		// turn it back off
		xthal_set_cpenable(0);
	}

	adc_rx_buffer[3] = (v >> 16) & 0xFF;
	adc_rx_buffer[4] = (v >>  8) & 0xFF;
	adc_rx_buffer[5] = (v >>  0) & 0xFF;

	adc_rx_buffer[6] = (c0 >> 16) & 0xFF;
	adc_rx_buffer[7] = (c0 >>  8) & 0xFF;
	adc_rx_buffer[8] = (c0 >>  0) & 0xFF;

	adc_handle_rx_data(adc_rx_buffer);

#if CONFIG_BOARD_TTGO
	gpio_set_level(18, 0);
#endif
	return false;
}
#endif /* !(HAVE_ADC) */

void adc_task(void* parameters)
{
	float *fbuf = NULL;
	size_t fbuf_idx = 0;
	uint32_t bufc_r_idx = 0;
	int32_t pv = 0;
	double psum = 0.0;
	uint32_t cnt = 0;
	int plc_ct = 0;

    ESP_LOGD(TAGA, "executing %s() on CORE%i\n", __func__, xPortGetCoreID());

	if (st7540_uart_init() != ESP_OK) {
		ESP_LOGE(TAGA, "st7540_uart_init failed, retrying in 1 second");
		vTaskDelay(1000 / portTICK_RATE_MS);
	}

	if (adc_init() != ESP_OK) {
		ESP_LOGE(TAGA, "adc_init() failed, retrying in 1 second");
		vTaskDelay(1000 / portTICK_RATE_MS);
	}

	for (;;) {
		while (adc_bufc_idx != bufc_r_idx) {
			int32_t v = adc_v_bufc[bufc_r_idx];
			int32_t c = adc_c_bufc[bufc_r_idx];
			if (fbuf && fbuf_idx < sizeof(adc_fbuf[0])/sizeof(adc_fbuf[0][0])) {
				fbuf[fbuf_idx + 0] = (float)v * (float)adc_V_scale;
				fbuf[fbuf_idx + 1] = (float)c * (float)adc_C_scale;
				fbuf_idx += 4;
			}
			if (pv <= 0 && v > 0) {
				if (cnt != 0) {
					adc_pwr = psum / (double)cnt * adc_V_scale * adc_C_scale;
				} else {
					adc_pwr = 0.0f;
				}
				if (fabsf(adc_pwr) < 1.0f) {
					plc_tx_Pwr = 0;
				} else if (adc_pwr >= 1.0f && adc_pwr < 10.0f) {
					plc_tx_Pwr = 1;
				} else if (adc_pwr >= 10.0f && adc_pwr < 100.0f) {
					plc_tx_Pwr = 2;
				} else if (adc_pwr >= 100.0f) {
					plc_tx_Pwr = 3;
				} else if (adc_pwr <= -1.0f && adc_pwr > -10.0f) {
					plc_tx_Pwr = -1;
				} else if (adc_pwr <= -10.0f && adc_pwr > -100.0f) {
					plc_tx_Pwr = -2;
				} else if (adc_pwr <= -100.0f && adc_pwr > -300.0f) {
					plc_tx_Pwr = -3;
				} else if (adc_pwr <= -300.0f) {
					plc_tx_Pwr = -4;
				}
				psum = 0.0;
				cnt = 0;
			}
			psum += (double)v * (double)c;
			cnt++;
			pv = v;
			bufc_r_idx = (bufc_r_idx + 1) & (sizeof(adc_v_bufc) / sizeof(adc_v_bufc[0]) - 1);
		}

		if (adc_50Hz_plc_trig) {
			adc_50Hz_plc_trig = 0;
			if (++plc_ct >= 2 /* 25Hz */) {
				plc_ct = 0;
				plc_handler();
			}
			/* fbuf */
			if (!fbuf || fbuf_idx == sizeof(adc_fbuf[0])/sizeof(adc_fbuf[0][0])) {
				fbuf = adc_try_get_next_fbuf();
				fbuf_idx = 0;
			}
		}
	}
}

esp_err_t adc_init()
{
    printf("executing %s() on CORE%i\n", __func__, xPortGetCoreID());

	adc_fbuf_init();

#if HAVE_ADC
	gpio_config_t io_conf;
    //interrupt of falling edge
    io_conf.intr_type = GPIO_INTR_NEGEDGE;
    //bit mask of the pins
    io_conf.pin_bit_mask = (1ull << ADC_DREADY_GPIO_NUM);
    //set as input mode
    io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);

    esp_err_t ret;
    spi_bus_config_t buscfg={
        .miso_io_num = ADC_MISO_GPIO_NUM,
        .mosi_io_num = ADC_MOSI_GPIO_NUM,
        .sclk_io_num = ADC_CLK_GPIO_NUM,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 32,
		.intr_flags = ESP_INTR_FLAG_IRAM
    };

    spi_device_interface_config_t devcfg={
        .clock_speed_hz = 4*1000*1000,           //Clock out at 20 MHz
        .mode = 1,                                //SPI mode 1
        .spics_io_num = -1,              //CS pin
        .queue_size = 2,                          //We want to be able to queue 2 transactions at a time
        .post_cb = adc_spi_post_transfer_callback,  //Specify post-transfer callback
    };
    //Initialize the SPI bus
    ret = spi_bus_initialize(ADC_HOST, &buscfg, SPI_DMA_CH_AUTO);
    ESP_ERROR_CHECK(ret);
    //Attach the ADC to the SPI bus
    ret = spi_bus_add_device(ADC_HOST, &devcfg, &adc_spi);
    ESP_ERROR_CHECK(ret);

    //Initialize the ADC
	adc_transaction.tx_buffer = adc_tx_buffer;               //Data
	adc_transaction.rx_buffer = adc_rx_buffer;

	// Try to reset and configure ADC
	for (;;) {
		vTaskDelay(20 / portTICK_RATE_MS);

		adc_transaction.length = 6 * 3 * 8;                 //Len is in bytes, transaction length is in bits.
		adc_tx_buffer[0] = H_BYTE(ADC_CMD_RESET); /* RESET */
		adc_tx_buffer[1] = L_BYTE(ADC_CMD_RESET); /* RESET */;
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);
		vTaskDelay(2 / portTICK_RATE_MS);

		adc_tx_buffer[0] = 0x00; /* NOP */
		adc_tx_buffer[1] = 0x00; /* NOP */;
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);
		if (adc_rx_buffer[0] != H_BYTE(ADC_ACK_RESET) || adc_rx_buffer[1] != L_BYTE(ADC_ACK_RESET)) {
			ESP_LOGE(TAGA, "Incorrect ACK for RESET command: 0x%02x%02x (should be 0x%04x)", (int)adc_rx_buffer[0], (int)adc_rx_buffer[1], ADC_ACK_RESET);
			continue;
		}

#if 0 /* Disabled because values are still manually assembled from bytes (incorrect byte order) */
		// After reset we have 24bit words in transactions, change to 32bit
		adc_tx_buffer[0] = 0x61; /* WREG MODE */
		adc_tx_buffer[1] = 0x00; /* WREG MODE */;
		adc_tx_buffer[2] = 0;
		adc_tx_buffer[3] = 0x02;
		adc_tx_buffer[4] = 0x10;
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);

		adc_transaction.length = 6 * 4 * 8;                 //Len is in bytes, transaction length is in bits.
		adc_tx_buffer[0] = 0x00; /* NOP */
		adc_tx_buffer[1] = 0x00; /* NOP */;
		adc_tx_buffer[2] = 0;
		adc_tx_buffer[3] = 0;
		adc_tx_buffer[4] = 0;
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);
		if (adc_rx_buffer[0] != 0x41 || adc_rx_buffer[1] != 0x00) {
			ESP_LOGE(TAGA, "Incorrect response for WRREG command: 0x%02x%02x (should be 0x4100)", (int)adc_rx_buffer[0], (int)adc_rx_buffer[1]);
			continue;
		}
#endif

		/* CLOCK = 0x0F0A */
		adc_tx_buffer[0] = H_BYTE(ADC_CMD_WREG(ADC_REG_CLOCK, 0));
		adc_tx_buffer[1] = L_BYTE(ADC_CMD_WREG(ADC_REG_CLOCK, 0));
		adc_tx_buffer[2] = 0;
		adc_tx_buffer[3] = H_BYTE(0x0F0A);
		adc_tx_buffer[4] = L_BYTE(0x0F0A);
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);

		adc_tx_buffer[0] = 0x00; /* NOP */
		adc_tx_buffer[1] = 0x00; /* NOP */;
		adc_tx_buffer[2] = 0;
		adc_tx_buffer[3] = 0;
		adc_tx_buffer[4] = 0;
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);
		if (adc_rx_buffer[0] != H_BYTE(ADC_ACK_WREG(ADC_REG_CLOCK, 0)) || adc_rx_buffer[1] != L_BYTE(ADC_ACK_WREG(ADC_REG_CLOCK, 0))) {
			ESP_LOGE(TAGA, "Incorrect response for WREG command: 0x%02x%02x (should be 0x%04x)", (int)adc_rx_buffer[0], (int)adc_rx_buffer[1],
					ADC_ACK_WREG(ADC_REG_CLOCK, 0));
			continue;
		}

		/* DCBLOCK = 0xC */
		adc_tx_buffer[0] = H_BYTE(ADC_CMD_WREG(ADC_REG_THRSHLD_LSB, 0));
		adc_tx_buffer[1] = L_BYTE(ADC_CMD_WREG(ADC_REG_THRSHLD_LSB, 0));
		adc_tx_buffer[2] = 0;
		adc_tx_buffer[3] = H_BYTE(0x000C);
		adc_tx_buffer[4] = L_BYTE(0x000C);
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);

		adc_tx_buffer[0] = 0x00; /* NOP */
		adc_tx_buffer[1] = 0x00; /* NOP */;
		adc_tx_buffer[2] = 0;
		adc_tx_buffer[3] = 0;
		adc_tx_buffer[4] = 0;
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);
		if (adc_rx_buffer[0] != H_BYTE(ADC_ACK_WREG(ADC_REG_THRSHLD_LSB, 0)) || adc_rx_buffer[1] != L_BYTE(ADC_ACK_WREG(ADC_REG_THRSHLD_LSB, 0))) {
			ESP_LOGE(TAGA, "Incorrect response for WREG command: 0x%02x%02x (should be 0x%04x)", (int)adc_rx_buffer[0], (int)adc_rx_buffer[1],
					ADC_ACK_WREG(ADC_REG_THRSHLD_LSB, 0));
			continue;
		}

		/* PHASE SHIFT ch1 */
		adc_tx_buffer[0] = H_BYTE(ADC_CMD_WREG(ADC_REG_CH1_CFG, 0));
		adc_tx_buffer[1] = L_BYTE(ADC_CMD_WREG(ADC_REG_CH1_CFG, 0));
		adc_tx_buffer[2] = 0;
		adc_tx_buffer[3] = H_BYTE(-418 << 6);
		adc_tx_buffer[4] = L_BYTE(-418 << 6);
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);

		adc_tx_buffer[0] = 0x00; /* NOP */
		adc_tx_buffer[1] = 0x00; /* NOP */;
		adc_tx_buffer[2] = 0;
		adc_tx_buffer[3] = 0;
		adc_tx_buffer[4] = 0;
		adc_transaction.rxlength = 0;
		ret=spi_device_polling_transmit(adc_spi, &adc_transaction);  //Transmit!
		ESP_ERROR_CHECK(ret);
		if (adc_rx_buffer[0] != H_BYTE(ADC_ACK_WREG(ADC_REG_CH1_CFG, 0)) || adc_rx_buffer[1] != L_BYTE(ADC_ACK_WREG(ADC_REG_CH1_CFG, 0))) {
			ESP_LOGE(TAGA, "Incorrect response for WREG command: 0x%02x%02x (should be 0x%04x)", (int)adc_rx_buffer[0], (int)adc_rx_buffer[1],
					ADC_ACK_WREG(ADC_REG_CH1_CFG, 0));
			continue;
		}

		break;
	}

#if 0
	for (int r = 0; r <= 9; r++) {
		if (r != 9) {
			tx_buffer[0] = 0xA0 | ((r >> 1) & 0x0f); /* RREG */
			tx_buffer[1] = (r & 1) << 7;
		} else {
			tx_buffer[0] = 0; /* NOP */
			tx_buffer[1] = 0;
			tx_buffer[2] = 0;
		}

		ret=spi_device_polling_transmit(adc_spi, &t);  //Transmit!
		ESP_ERROR_CHECK(ret);

		printf("Data from ADC transaction %i:\n", r);
		for (int i = 0; i < 6 * 4; i++) {
			printf("%02x ", (int)rx_buffer[i]);
			if (i % 4 == 3) puts("");
		}
		puts("\n");
	}
#endif



	/*
	t.length = 6 * 4 * 8;
	t.tx_buffer = dummy;
	t.rx_buffer = rx_buffer;
    ret=spi_device_polling_transmit(adc_spi, &t);  //Transmit!
    ESP_ERROR_CHECK(ret);
	puts("Data from second ADC transaction:");
	for (int i = 0; i < 6; i++) {
		printf("0x%02x%02x%02x%02x\n", (int)rx_buffer[i * 4 + 0], (int)rx_buffer[i * 4 + 1], (int)rx_buffer[i * 4 + 2], (int)rx_buffer[i * 4 + 2]);
	}
	puts("");
	*/

	memset(adc_tx_buffer, 0, sizeof(adc_tx_buffer));
	adc_data_ct = -1;
    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_IRAM);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(ADC_DREADY_GPIO_NUM, adc_drdy_isr_handler, (void*)0);

#else
	/* ADC emulation with timer interrupt */
#	if (ESP_IDF_VERSION_MAJOR == 5)
	gptimer_handle_t gptimer = NULL;
	gptimer_config_t timer_config = {
		.clk_src = GPTIMER_CLK_SRC_DEFAULT,
		.direction = GPTIMER_COUNT_UP,
		.resolution_hz = 1000000,
	};
	ESP_ERROR_CHECK(gptimer_new_timer(&timer_config, &gptimer));
	gptimer_event_callbacks_t cbs = {
		.on_alarm = adc_emul_handler,
	};
	ESP_ERROR_CHECK(gptimer_register_event_callbacks(gptimer, &cbs, NULL));
	ESP_ERROR_CHECK(gptimer_enable(gptimer));
	gptimer_alarm_config_t alarm_config = {
		.reload_count = 0,
		.alarm_count = 1000000 / 4000,
		.flags.auto_reload_on_alarm = true,
	};
	ESP_ERROR_CHECK(gptimer_set_alarm_action(gptimer, &alarm_config));
	ESP_ERROR_CHECK(gptimer_start(gptimer));
#	else /* ESP_IDF_VERSION_MAJOR != 5 */
	timer_config_t tcfg = {
		.divider = 16,
		.counter_dir = TIMER_COUNT_UP,
		.counter_en = TIMER_PAUSE,
		.alarm_en = TIMER_ALARM_EN,
		.auto_reload = true,
	};
	ESP_LOGI(TAGA, "Before timer_init");
	ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &tcfg));
	ESP_LOGI(TAGA, "After timer_init");
	ESP_ERROR_CHECK(timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, (TIMER_BASE_CLK / 16) / 4000));
	ESP_LOGI(TAGA, "After timer_set_alarm_value");
	ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
	ESP_LOGI(TAGA, "After timer_enable_intr");
	ESP_ERROR_CHECK(timer_isr_callback_add(TIMER_GROUP_0, TIMER_0, adc_emul_handler, NULL, 0));
	ESP_LOGI(TAGA, "After timer_isr_callback_add");
	ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));
	ESP_LOGI(TAGA, "After timer_start");
#	endif /* ESP_IDF_VERSION_MAJOR == 5 */
#endif /* HAVE_ADC */

	return ESP_OK;
}

#if REPLACE_SPI_FUNCTIONS

static IRAM_ATTR void adc_spi_uninstall_priv_desc(spi_trans_priv_t* trans_buf)
{
    spi_transaction_t *trans_desc = trans_buf->trans;
    if ((void *)trans_buf->buffer_to_send != &trans_desc->tx_data[0] &&
        trans_buf->buffer_to_send != trans_desc->tx_buffer) {
        free((void *)trans_buf->buffer_to_send); //force free, ignore const
    }
    // copy data from temporary DMA-capable buffer back to IRAM buffer and free the temporary one.
    if ((void *)trans_buf->buffer_to_rcv != &trans_desc->rx_data[0] &&
        trans_buf->buffer_to_rcv != trans_desc->rx_buffer) { // NOLINT(clang-analyzer-unix.Malloc)
        if (trans_desc->flags & SPI_TRANS_USE_RXDATA) {
            memcpy((uint8_t *) & trans_desc->rx_data[0], trans_buf->buffer_to_rcv, (trans_desc->rxlength + 7) / 8);
        } else {
            memcpy(trans_desc->rx_buffer, trans_buf->buffer_to_rcv, (trans_desc->rxlength + 7) / 8);
        }
        free(trans_buf->buffer_to_rcv);
    }
}

static IRAM_ATTR esp_err_t adc_spi_setup_priv_desc(spi_transaction_t *trans_desc, spi_trans_priv_t* new_desc, bool isdma)
{
    *new_desc = (spi_trans_priv_t) { .trans = trans_desc, };

    // rx memory assign
    uint32_t* rcv_ptr;
    if ( trans_desc->flags & SPI_TRANS_USE_RXDATA ) {
        rcv_ptr = (uint32_t *)&trans_desc->rx_data[0];
    } else {
        //if not use RXDATA neither rx_buffer, buffer_to_rcv assigned to NULL
        rcv_ptr = trans_desc->rx_buffer;
    }
    if (rcv_ptr && isdma && (!esp_ptr_dma_capable(rcv_ptr) || ((int)rcv_ptr % 4 != 0))) {
        //if rxbuf in the desc not DMA-capable, malloc a new one. The rx buffer need to be length of multiples of 32 bits to avoid heap corruption.
        ESP_LOGD(TAGA, "Allocate RX buffer for DMA" );
        rcv_ptr = heap_caps_malloc((trans_desc->rxlength + 31) / 8, MALLOC_CAP_DMA);
        if (rcv_ptr == NULL) goto clean_up;
    }
    new_desc->buffer_to_rcv = rcv_ptr;

    // tx memory assign
    const uint32_t *send_ptr;
    if ( trans_desc->flags & SPI_TRANS_USE_TXDATA ) {
        send_ptr = (uint32_t *)&trans_desc->tx_data[0];
    } else {
        //if not use TXDATA neither tx_buffer, tx data assigned to NULL
        send_ptr = trans_desc->tx_buffer ;
    }
    if (send_ptr && isdma && !esp_ptr_dma_capable( send_ptr )) {
        //if txbuf in the desc not DMA-capable, malloc a new one
        ESP_LOGD(TAGA, "Allocate TX buffer for DMA" );
        uint32_t *temp = heap_caps_malloc((trans_desc->length + 7) / 8, MALLOC_CAP_DMA);
        if (temp == NULL) goto clean_up;

        memcpy( temp, send_ptr, (trans_desc->length + 7) / 8 );
        send_ptr = temp;
    }
    new_desc->buffer_to_send = send_ptr;

    return ESP_OK;

clean_up:
    adc_spi_uninstall_priv_desc(new_desc);
    return ESP_ERR_NO_MEM;
}

SPI_MASTER_ATTR static inline void adc_spi_req_core(spi_bus_lock_dev_t *dev_handle)
{
    spi_bus_lock_t *lock = dev_handle->parent;

    // Though `acquired_dev` is critical resource, `dev_handle == lock->acquired_dev`
    // is a stable statement unless `acquire_start` or `acquire_end` is called by current
    // device.
    if (dev_handle == lock->acquiring_dev){
        // Set the REQ bit and check BG bits if we are the acquiring processor.
        // If the BG bits were not active before, invoke the BG again.

        // Avoid competitive risk against the `clear_pend_core`, `acq_dev_bg_active` should be set before
        // setting REQ bit.
        lock->acq_dev_bg_active = true;
        uint32_t status = atomic_fetch_or(&lock->status, DEV_REQ_MASK(dev_handle));
        if ((status & DEV_BG_MASK(dev_handle)) == 0) {
            lock->bg_enable(lock->bg_arg); //acquiring processor passed to BG
        }
    } else {
        uint32_t status = atomic_fetch_or(&lock->status, DEV_REQ_MASK(dev_handle));
        if (status == 0) {
            lock->bg_enable(lock->bg_arg); //acquiring processor passed to BG
        }
    }
}

esp_err_t IRAM_ATTR spi_device_queue_trans_fromISR(spi_device_handle_t handle, spi_transaction_t *trans_desc, TickType_t ticks_to_wait)
{
    esp_err_t ret;

	/* Supposedly transaction is valid
    ret = check_trans_valid(handle, trans_desc);
    if (ret != ESP_OK) return ret;
	*/

    spi_host_t *host = handle->host;

	/* And device is not polling
    SPI_CHECK(!spi_bus_device_is_polling(handle), "Cannot queue new transaction while previous polling transaction is not terminated.", ESP_ERR_INVALID_STATE );
	*/

    spi_trans_priv_t trans_buf;
    ret = adc_spi_setup_priv_desc(trans_desc, &trans_buf, (host->bus_attr->dma_enabled));
    if (ret != ESP_OK) return ret;

#ifdef CONFIG_PM_ENABLE
    esp_pm_lock_acquire(host->bus_attr->pm_lock);
#endif
    //Send to queue and invoke the ISR.

    BaseType_t r = xQueueSendFromISR(handle->trans_queue, (void *)&trans_buf, NULL);
    if (!r) {
        ret = ESP_ERR_TIMEOUT;
#ifdef CONFIG_PM_ENABLE
        //Release APB frequency lock
        esp_pm_lock_release(host->bus_attr->pm_lock);
#endif
        goto clean_up;
    }

    // The ISR will be invoked at correct time by the lock with `spi_bus_intr_enable`.
    adc_spi_req_core(handle->dev_lock);

    return ESP_OK;

clean_up:
    adc_spi_uninstall_priv_desc(&trans_buf);
    return ret;
}

#endif

/* vim: set ts=4 sw=4 noet nowrap: */
