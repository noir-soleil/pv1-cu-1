#ifndef _INCLUDED_OTA_H_
#	define _INCLUDED_OTA_H_

#include "esp_http_server.h"

void ota_register_handlers(httpd_config_t *cfg);
void ota_register_uris(httpd_handle_t server);

#endif /* ndef _INCLUDED_OTA_H_ */

