#ifndef _INCLUDED_FONT_H_
#	define _INCLUDED_FONT_H_

#include <stdint.h>

struct font {
	uint8_t version;
	uint8_t size;
	struct {
		int8_t w, h, offx, offy;
	} bbx;
	uint8_t asc, dsc;
	uint32_t default_char;
	uint8_t sp;
	const void * data;
};

uint32_t font_find_char(const struct font * font, unsigned int c);

static inline unsigned char font_glyph_bit(const struct font *font, uint32_t offset, unsigned int row, unsigned int col)
{
	if (font->version == 0) {
		if (row >= font->bbx.h) return 0;
		if (col >= font->bbx.w) return 0;

		unsigned int lbt = ((font->bbx.w + 7) >> 3);
		const unsigned char *b = (const unsigned char*)font->data + offset;
		unsigned char d = b[lbt * row + (col >> 3)];
		return (unsigned char)((d >> (7 - (col & 0x7))) & 0x1);
	}

	return 0;
}

#endif /* ndef _INCLUDED_FONT_H_ */
