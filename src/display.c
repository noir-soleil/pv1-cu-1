#include "display.h"
#include "hardware.h"

#include <stdint.h>
#include <string.h>
#include "esp_system.h"
#include "esp_log.h"
#if OLED_IFACE_SPI
#include "driver/spi_master.h"
#endif
#if OLED_IFACE_I2C
#include "driver/i2c.h"
#endif
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h" /* portTICK_RATE_MS */
#if !defined(portTICK_RATE_MS)
#	define portTICK_RATE_MS portTICK_PERIOD_MS
#endif
#include "freertos/task.h" /* vTaskDelay */

#if HAVE_OLED
static const char *TAG = "OLED";
#endif /* HAVE_OLED */

#if HAVE_OLED
static uint8_t *oled_fb = NULL;
#endif /* HAVE_OLED */

#if OLED_IFACE_SPI
typedef spi_device_handle_t oled_handle_t;
#	define OLED_HANDLE_INVALID NULL
#elif OLED_IFACE_I2C
typedef i2c_port_t oled_handle_t;
#	define OLED_HANDLE_INVALID I2C_NUM_MAX
#endif

oled_handle_t oled_handle = OLED_HANDLE_INVALID;

/*
 The LCD needs a bunch of command/argument values to be initialized. They are stored in this struct.
*/
typedef struct {
    uint8_t cmd;
    uint8_t data[8];
    uint8_t databytes; //No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} oled_init_cmd_t;

#if HAVE_OLED
#	if OLED_SSD1309
//Place data into DRAM. Constant data gets placed into DROM by default, which is not accessible by DMA.
DRAM_ATTR static const oled_init_cmd_t oled_init_cmds[]={
	/* Command Lock */
	{0xFD, {0x12}, 1},
	/* Set Display Off */
	{0xAE, {0}, 0},
	/* Set Display Clock Divide Ratio/Oscillator Frequency */
	{0xD5, {0xA0}, 1},
	/* Set Multiplex Ratio */
	{0xA8, {0x3F}, 1},
	/* Set Display Offset */
	{0xD3, {0x00}, 1},
	/* Set Display Start Line */
	{0x40, {0}, 0},
	/* Set Segment Re-Map */
	{0xA1, {0}, 0},
	/* Set COM Output Scan Direction */
	{0xC8, {0}, 0},
	/* Set COM Pins Hardware Configuration */
	{0xDA, {0x12}, 1},
	/* Set Current Control */
	{0x81, {0xCF}, 1},
	/* Set Pre-Charge Period */
	{0xD9, {0x22}, 1},
	/* Set VCOMH Deselect Level */
	{0xDB, {0x34}, 1},
	/* Set Entire Display On/Off */
	{0xA4, {0}, 0},
	/* Set Normal/Inverse Display */
	{0xA6, {0}, 0},
	{0, {0}, 0xff}
};
#	elif OLED_SSD1306
//Place data into DRAM. Constant data gets placed into DROM by default, which is not accessible by DMA.
DRAM_ATTR static const oled_init_cmd_t oled_init_cmds[]={
	/* Command Lock */
	{0xFD, {0x12}, 1},
	/* Set Display Off */
	{0xAE, {0}, 0},
	/* Set Display Clock Divide Ratio/Oscillator Frequency */
	{0xD5, {0xA0}, 1},
	/* Set Multiplex Ratio */
	{0xA8, {0x3F}, 1},
	/* Set Display Offset */
	{0xD3, {0x00}, 1},
	/* Set Display Start Line */
	{0x40, {0}, 0},
	/* Set charge pump */
	{0x8D, {0x14}, 1},
	/* Set Segment Re-Map */
	{0xA1, {0}, 0},
	/* Set COM Output Scan Direction */
	{0xC8, {0}, 0},
	/* Set COM Pins Hardware Configuration */
	{0xDA, {0x12}, 1},
	/* Set Current Control */
	{0x81, {0xCF}, 1},
	/* Set Pre-Charge Period */
	{0xD9, {0xF1}, 1},
	/* Set VCOMH Deselect Level */
	{0xDB, {0x40}, 1},
	/* Set Entire Display On/Off */
	{0xA4, {0}, 0},
	/* Set Normal/Inverse Display */
	{0xA6, {0}, 0},
	{0, {0}, 0xff}
};
#	endif /* OLED_SSD1309 */

static esp_err_t oled_cmd_byte(oled_handle_t oled, const uint8_t cmd)
{
	esp_err_t ret = ESP_FAIL;
#	if OLED_IFACE_SPI
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length = 8;                 //Len is in bytes, transaction length is in bits.
    t.tx_buffer = &cmd;               //Data
    t.user = (void *)0;                //D/C needs to be set to 0
    ret = spi_device_polling_transmit(oled, &t);  //Transmit!
#	elif OLED_IFACE_I2C
	i2c_cmd_handle_t i2c_chain;
	if ((i2c_chain = i2c_cmd_link_create()) == NULL) return ESP_FAIL;
	if ((ret = i2c_master_start(i2c_chain)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_write_byte(i2c_chain, (OLED_I2C_ADDR << 1) | I2C_MASTER_WRITE, true /* ACK check */)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_write_byte(i2c_chain, 0x80, true /* ACK check */)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_write_byte(i2c_chain, cmd, true /* ACK check */)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_stop(i2c_chain)) != ESP_OK) goto cleanup;

	ret = i2c_master_cmd_begin(oled, i2c_chain, 10 / portTICK_RATE_MS);
cleanup:
	i2c_cmd_link_delete(i2c_chain);
#	endif
	return ret;
}

static esp_err_t oled_cmd(oled_handle_t oled, const void *data, int len)
{
    if (len==0) return ESP_OK;             //no need to send anything
    esp_err_t ret = ESP_FAIL;

#	if OLED_IFACE_SPI
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length = len * 8;                 //Len is in bytes, transaction length is in bits.
    t.tx_buffer = data;               //Data
    t.user = (void *)0;                //D/C needs to be set to 0
    ret = spi_device_polling_transmit(oled, &t);  //Transmit!
#	elif OLED_IFACE_I2C
	i2c_cmd_handle_t i2c_chain;
	if ((i2c_chain = i2c_cmd_link_create()) == NULL) return ESP_FAIL;
	if ((ret = i2c_master_start(i2c_chain)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_write_byte(i2c_chain, (OLED_I2C_ADDR << 1) | I2C_MASTER_WRITE, true /* ACK check */)) != ESP_OK) goto cleanup;
	for (int i = 0; i < len; ++i) {
		if ((ret = i2c_master_write_byte(i2c_chain, 0x80, true /* ACK check */)) != ESP_OK) goto cleanup;
		if ((ret = i2c_master_write_byte(i2c_chain, ((uint8_t *)data)[i], true /* ACK check */)) != ESP_OK) goto cleanup;
	}
	if ((ret = i2c_master_stop(i2c_chain)) != ESP_OK) goto cleanup;

	ret = i2c_master_cmd_begin(oled, i2c_chain, 10 / portTICK_RATE_MS);
cleanup:
	i2c_cmd_link_delete(i2c_chain);
#	endif
	return ret;
}

static esp_err_t oled_data(oled_handle_t oled, const void *data, int len)
{
    if (len==0) return ESP_OK;             //no need to send anything
    esp_err_t ret = ESP_FAIL;

#	if OLED_IFACE_SPI
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length = len * 8;                 //Len is in bytes, transaction length is in bits.
    t.tx_buffer = data;               //Data
    t.user = (void *)1;                //D/C needs to be set to 1
    ret = spi_device_polling_transmit(oled, &t);  //Transmit!
#	elif OLED_IFACE_I2C
	i2c_cmd_handle_t i2c_chain;
	if ((i2c_chain = i2c_cmd_link_create()) == NULL) return ESP_FAIL;
	if ((ret = i2c_master_start(i2c_chain)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_write_byte(i2c_chain, (OLED_I2C_ADDR << 1) | I2C_MASTER_WRITE, true /* ACK check */)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_write_byte(i2c_chain, 0x40, true /* ACK check */)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_write(i2c_chain, data, len, true /* ACK check */)) != ESP_OK) goto cleanup;
	if ((ret = i2c_master_stop(i2c_chain)) != ESP_OK) goto cleanup;

	ret = i2c_master_cmd_begin(oled, i2c_chain, (len >> 5) / portTICK_RATE_MS + 1);
cleanup:
	i2c_cmd_link_delete(i2c_chain);
#	endif
	return ret;
}

#	if OLED_IFACE_SPI
//This function is called (in irq context!) just before a transmission starts. It will
//set the D/C line to the value indicated in the user field.
void oled_spi_pre_transfer_callback(spi_transaction_t *t)
{
    int dc=(int)t->user;
    gpio_set_level(OLED_DC_GPIO, dc);
}
#	endif /* OLED_IFACE_SPI */

//Initialize the display
void oled_init_display(oled_handle_t oled, const oled_init_cmd_t *cmds)
{
    int cmd=0;

    //Send all the commands
    while (cmds[cmd].databytes!=0xff) {
        oled_cmd_byte(oled, cmds[cmd].cmd);
        oled_cmd(oled, cmds[cmd].data, cmds[cmd].databytes & 0x07);
        if (cmds[cmd].databytes & 0x80) {
            vTaskDelay(100 / portTICK_RATE_MS);
        }
        cmd++;
    }

	/* Set horizontal addressing mode */
	oled_cmd(oled, "\x20\x00", 2);
    /* Clear screen */
	display_update_area(0, 0, OLED_W, OLED_H);

#	if defined(OLED_VCEN_GPIO)
    gpio_set_level(OLED_VCEN_GPIO, 1);
    vTaskDelay(100 / portTICK_RATE_MS);
#	endif /* defined(OLED_VCEN_GPIO) */

    oled_cmd_byte(oled, 0xAF);
}

static void oled_fb_draw_char(const unsigned int c, const struct font *font, const unsigned int x, const unsigned int y, int inv)
{
	if (!font) return;
	if (x >= OLED_W) return;
	if (y >= OLED_H) return;
	uint32_t coffset = font_find_char(font, c);
	for (int ro = 0; ro < font->bbx.h; ++ro) {
		int fbro = ro + y;
		if (fbro >= OLED_H) break;
		for (int co = 0; co < font->bbx.w; ++co) {
			int fbco = co + x;
			if (fbco >= OLED_W) break;
			unsigned char fbb = oled_fb[(fbro >> 3) * OLED_W + fbco];
			unsigned char b = (font_glyph_bit(font, coffset, ro, co) << (fbro & 0x7));
			if (inv) b ^= (1 << (fbro & 0x7));
			fbb = (fbb & ~(1 << (fbro & 0x7))) | b;
			oled_fb[(fbro >> 3) * OLED_W + fbco] = fbb;
		}
	}
}

#endif /* HAVE_OLED */

void display_draw_string_fb(const char *s, const struct font *font, unsigned int x, unsigned int y, int inv)
{
#if HAVE_OLED
	if (oled_handle == OLED_HANDLE_INVALID) return;
	if (y >= OLED_H) return;
	unsigned int xend = x;
	while (*s && xend < OLED_W) {
		uint32_t c;
		c = *s++;
		if (c & 0x80) {
			if ((c & 0xE0) == 0xC0) {
				c = ((c & 0x1F) << 6) | (*s++ & 0x3F);
			} else if ((c & 0xF0) == 0xE0) {
				c = ((c & 0x0F) << 6) | (*s++ & 0x3F);
				c = (c << 6) | (*s++ & 0x3F);
			} else if ((c & 0xF8) == 0xF0) {
				c = ((c & 0x07) << 6) | (*s++ & 0x3F);
				c = (c << 6) | (*s++ & 0x3F);
				c = (c << 6) | (*s++ & 0x3F);
			} else {
				c = font->default_char;
				++s;
			}
		}
		oled_fb_draw_char(c, font, xend, y, inv);
		/* FIXME: font->sp should be filled with bg color */
		xend += font->bbx.w + font->sp;
	}
#endif /* HAVE_OLED */
}

void display_draw_string(const char *s, const struct font *font, unsigned int x, unsigned int y, int inv)
{
	display_draw_string_fb(s, font, x, y, inv);
	display_update_area(x, y, font->bbx.w * strlen(s), font->bbx.h);
}

void display_fill_area_fb(unsigned int x, unsigned int y, unsigned int w, unsigned int h, int v)
{
#if HAVE_OLED
	if (oled_handle == OLED_HANDLE_INVALID) return;
	if (x >= OLED_W) return;
	if (y >= OLED_H) return;
	for (int ro = 0; ro < h; ++ro) {
		int fbro = ro + y;
		if (fbro >= OLED_H) break;
		for (int co = 0; co < w; ++co) {
			int fbco = co + x;
			if (fbco >= OLED_W) break;
			unsigned char fbb = oled_fb[(fbro >> 3) * OLED_W + fbco];
			fbb = (fbb & ~(1 << (fbro & 0x7))) | (v ? (1 << (fbro & 0x7)) : 0);
			oled_fb[(fbro >> 3) * OLED_W + fbco] = fbb;
		}
	}
#endif /* HAVE_OLED */
}

void display_fill_area(unsigned int x, unsigned int y, unsigned int w, unsigned int h, int v)
{
	display_fill_area_fb(x, y, w, h, v);
	display_update_area(x, y, w, h);
}

void display_update_area(unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	if (oled_handle == OLED_HANDLE_INVALID) return;
	if (x >= OLED_W) return;
	if (y >= OLED_H) return;
	static uint8_t cmds[] = {0x21, 0, OLED_W - 1, 0x22, 0, (OLED_H / 8) - 1};
	cmds[1] = (uint8_t)x;
	cmds[2] = (((x + w) > OLED_W) ? OLED_W : (x + w)) - 1;
	cmds[4] = y >> 3;
	cmds[5] = ((((y + h) > OLED_H) ? OLED_H : y + h) - 1) >> 3;
	oled_cmd(oled_handle, cmds, sizeof(cmds));
    for (uint8_t p = cmds[4]; p <= cmds[5]; ++p) {
	    oled_data(oled_handle, &oled_fb[OLED_W * (int)p + (int)cmds[1]], cmds[2] - cmds[1] + 1);
    }
}

esp_err_t display_init()
{
    ESP_LOGD(TAG, "executing %s() on CORE%i", __func__, xPortGetCoreID());
#if HAVE_OLED
	oled_fb = calloc(OLED_W * OLED_H / 8, 1);
	if (!oled_fb) return ESP_FAIL;
	esp_err_t ret;
#	if OLED_IFACE_SPI
    gpio_set_direction(OLED_DC_GPIO, GPIO_MODE_OUTPUT);
#	if defined(OLED_VCEN_GPIO)
    gpio_set_direction(OLED_VCEN_GPIO, GPIO_MODE_OUTPUT);
#	endif /* defined(OLED_VCEN_GPIO) */

    spi_bus_config_t buscfg={
        .miso_io_num = -1,
        .mosi_io_num = OLED_SDIN_GPIO,
        .sclk_io_num = OLED_SCLK_GPIO,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = /* TODO: */OLED_W
    };

    spi_device_interface_config_t devcfg={
        .clock_speed_hz = 8*1000*1000,           //Clock out at 5 MHz
        .mode = 0,                                //SPI mode 0
        .spics_io_num = OLED_CS_GPIO,              //CS pin
        .queue_size = 7,                          //We want to be able to queue 7 transactions at a time
        .pre_cb = oled_spi_pre_transfer_callback,  //Specify pre-transfer callback to handle D/C line
    };
    //Initialize the SPI bus
    if ((ret = spi_bus_initialize(OLED_SPI_HOST, &buscfg, SPI_DMA_CH_AUTO)) != ESP_OK) return ret;
    //Attach the LCD to the SPI bus
    if ((ret = spi_bus_add_device(OLED_SPI_HOST, &devcfg, &oled_handle)) != ESP_OK) return ret;
    //Initialize the LCD
#	elif OLED_IFACE_I2C
	i2c_config_t i2c_conf = {
		.mode = I2C_MODE_MASTER,
		.sda_io_num = OLED_SDA_GPIO,
		.sda_pullup_en = GPIO_PULLUP_DISABLE,
		.scl_io_num = OLED_SCL_GPIO,
		.scl_pullup_en = GPIO_PULLUP_DISABLE,
		.master.clk_speed = 400000,
		.clk_flags = 0,
	};
	if ((ret = i2c_param_config(OLED_I2C, &i2c_conf)) != ESP_OK) return ret;
	if ((ret = i2c_driver_install(OLED_I2C, i2c_conf.mode, 0, 0, 0)) != ESP_OK) return ret;
	oled_handle = OLED_I2C;
#	endif /* OLED_IFACE_SPI */
    oled_init_display(oled_handle, oled_init_cmds);
#endif /* HAVE_OLED */

	return ESP_OK;
}

void display_invert(int inv)
{
#if HAVE_OLED
	if (oled_handle == OLED_HANDLE_INVALID) return;
	if (inv) {
		oled_cmd_byte(oled_handle, 0xA7);
	} else {
		oled_cmd_byte(oled_handle, 0xA6);
	}
#endif /* HAVE_OLED */
}

#if 0
/* To send a set of lines we have to send a command, 2 data bytes, another command, 2 more data bytes and another command
 * before sending the line data itself; a total of 6 transactions. (We can't put all of this in just one transaction
 * because the D/C line needs to be toggled in the middle.)
 * This routine queues these commands up as interrupt transactions so they get
 * sent faster (compared to calling spi_device_transmit several times), and at
 * the mean while the lines for next transactions can get calculated.
 */
static void send_lines(spi_device_handle_t spi, int ypos, uint16_t *linedata)
{
    esp_err_t ret;
    int x;
    //Transaction descriptors. Declared static so they're not allocated on the stack; we need this memory even when this
    //function is finished because the SPI driver needs access to it even while we're already calculating the next line.
    static spi_transaction_t trans[6];

    //In theory, it's better to initialize trans and data only once and hang on to the initialized
    //variables. We allocate them on the stack, so we need to re-init them each call.
    for (x=0; x<6; x++) {
        memset(&trans[x], 0, sizeof(spi_transaction_t));
        if ((x&1)==0) {
            //Even transfers are commands
            trans[x].length=8;
            trans[x].user=(void*)0;
        } else {
            //Odd transfers are data
            trans[x].length=8*4;
            trans[x].user=(void*)1;
        }
        trans[x].flags=SPI_TRANS_USE_TXDATA;
    }
    trans[0].tx_data[0]=0x2A;           //Column Address Set
    trans[1].tx_data[0]=0;              //Start Col High
    trans[1].tx_data[1]=0;              //Start Col Low
    trans[1].tx_data[2]=(320)>>8;       //End Col High
    trans[1].tx_data[3]=(320)&0xff;     //End Col Low
    trans[2].tx_data[0]=0x2B;           //Page address set
    trans[3].tx_data[0]=ypos>>8;        //Start page high
    trans[3].tx_data[1]=ypos&0xff;      //start page low
    trans[3].tx_data[2]=(ypos+PARALLEL_LINES)>>8;    //end page high
    trans[3].tx_data[3]=(ypos+PARALLEL_LINES)&0xff;  //end page low
    trans[4].tx_data[0]=0x2C;           //memory write
    trans[5].tx_buffer=linedata;        //finally send the line data
    trans[5].length=320*2*8*PARALLEL_LINES;          //Data length, in bits
    trans[5].flags=0; //undo SPI_TRANS_USE_TXDATA flag

    //Queue all transactions.
    for (x=0; x<6; x++) {
        ret=spi_device_queue_trans(spi, &trans[x], portMAX_DELAY);
        assert(ret==ESP_OK);
    }

    //When we are here, the SPI driver is busy (in the background) getting the transactions sent. That happens
    //mostly using DMA, so the CPU doesn't have much to do here. We're not going to wait for the transaction to
    //finish because we may as well spend the time calculating the next line. When that is done, we can call
    //send_line_finish, which will wait for the transfers to be done and check their status.
}


static void send_line_finish(spi_device_handle_t spi)
{
    spi_transaction_t *rtrans;
    esp_err_t ret;
    //Wait for all 6 transactions to be done and get back the results.
    for (int x=0; x<6; x++) {
        ret=spi_device_get_trans_result(spi, &rtrans, portMAX_DELAY);
        assert(ret==ESP_OK);
        //We could inspect rtrans now if we received any info back. The LCD is treated as write-only, though.
    }
}
#endif

/* vim: set ts=4 sw=4 noet nowrap: */
