#ifndef _INCLUDED_HARDWARE_H_
#	define _INCLUDED_HARDWARE_H_

#	include "board.h"

#	if (!BOARD_PV1_CUPCBTOP && !BOARD_TTGO)
#		error "Board should be defined, see hardware.h"
#	endif

#	if (BOARD_PV1_CUPCBTOP)

#		define HAVE_OLED 1
#		define OLED_SSD1309 1
#		define OLED_IFACE_SPI 1

#		define OLED_SPI_HOST VSPI_HOST
#		define OLED_SCLK_GPIO 18
#		define OLED_SDIN_GPIO 21
#		define OLED_CS_GPIO   19
#		define OLED_DC_GPIO   0
#		define OLED_VCEN_GPIO 4

#		define OLED_W 128
#		define OLED_H 64

#	elif (BOARD_TTGO)

#		define HAVE_OLED 1
#		define OLED_SSD1306 1
#		define OLED_IFACE_I2C 1
#		define OLED_I2C 0

#		define OLED_SCL_GPIO 22
#		define OLED_SDA_GPIO 21
#		define OLED_I2C_ADDR 0x3C

#		define OLED_W 128
#		define OLED_H 64

#		define POWER_ON_GPIO 26


#	endif /* BOARD_PV1_CUPCBTOP */

#endif /* ndef _INCLUDED_HARDWARE_H_ */

/* vim: set ts=4 sw=4 noet nowrap cin ai: */
