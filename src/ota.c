#include "ota.h"
#include "sdkconfig.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "adc.h"
#include "plc.h"
#include "crc32.h"

/* 
 * HTTPD is a single thread, so we should not worry that new OTA will start
 * until current is complete. But it can be started before reboot is executed
 */

#define OTA_DATA_BUFFER_SIZE 4096
#define OTA_PU_BUFFER_SIZE (80 * 1024)
#define OTA_PU_TIMEOUT (5000 / portTICK_RATE_MS)
#ifndef MIN
#	define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

static const char *TAG = "OTA";
static const char *TAGhttp = "OTA:HTTP";
static const char *TAGws = "OTA:WS";

static bool ota_reboot_sheduled = false;
static char *ota_buffer = NULL;
static char *PU_ota_buffer = NULL;
static size_t ota_buffer_pos;
static const esp_partition_t *ota_partition = NULL;
static esp_ota_handle_t ota_handle;
static char ota_error_buf[129] = {0};
static char ota_msg_buf[256] = {0};
static size_t ota_image_len = 0;

static int ota_wsfds[CONFIG_LWIP_MAX_LISTENING_TCP] = {0};
static httpd_handle_t ota_httpServer;
static httpd_close_func_t ota_prevCloseFunc = NULL;

void ota_reboot_task(void *parameters)
{
	vTaskDelay(1000 / portTICK_RATE_MS);
	esp_restart();
}

static void ota_do_reboot()
{
	ota_reboot_sheduled = true;
	xTaskCreatePinnedToCore(&ota_reboot_task, "OTAreboot_task", 1024, NULL,
			tskIDLE_PRIORITY, NULL, xPortGetCoreID());
}

enum ota_err_t {
	otaErr_INCORRECT_PARAM = -1,
	otaErr_ZERO_SZ = -2,
	otaErr_NO_MEM = -3,
	otaErr_REBOOT_PENDING = -4,
	otaErr_NO_PARTITION = -5,
	otaErr_BEGIN_FAILED = -6,
	otaErr_WRITE_FAILED = -7,
	otaErr_END_FAILED = -8,
	otaErr_SET_PART_FAILED = -9,
	otaErr_FILE_TOO_LARGE = -10,
	otaErr_FILE_INCORRECT = -11,
	otaErr_FILE_LEN_INCORRECT = -12,
	otaErr_ERASE_FAILED = -13,
	otaErr_PROGRAM_FAILED = -14,
	otaErr_TIMEOUT = -15,
	otaErr_VERIFY_CHECKSUM = -16,
};

const char *ota_err_str(enum ota_err_t err)
{
	switch (err) {
		case otaErr_INCORRECT_PARAM:
			return "EO: Incorrect parameters given";
		case otaErr_ZERO_SZ:
			return "EO: Incorrect parameters given: sz == 0 in the beginning";
		case otaErr_NO_MEM:
			return "EO: Unable to allocate OTA data buffer";
		case otaErr_REBOOT_PENDING:
			return "EO: Reboot is sheduled by other OTA process";
		case otaErr_NO_PARTITION:
			return "EO: Passive OTA partition not found";
		case otaErr_BEGIN_FAILED:
			if (ota_error_buf[0]) return ota_error_buf;
			else return "EO: esp_ota_begin() failed (?)";
		case otaErr_WRITE_FAILED:
			if (ota_error_buf[0]) return ota_error_buf;
			else return "EO: esp_ota_write() failed (?)";
		case otaErr_END_FAILED:
			if (ota_error_buf[0]) return ota_error_buf;
			else return "EO: esp_ota_end() failed (?)";
		case otaErr_SET_PART_FAILED:
			if (ota_error_buf[0]) return ota_error_buf;
			else return "EO: esp_ota_set_boot_partition() failed (?)";
		case otaErr_FILE_TOO_LARGE:
			return "EO: File too large";
		case otaErr_FILE_INCORRECT:
			return "EO: File is not sutable neither for INV nor for LCC";
		case otaErr_FILE_LEN_INCORRECT:
			return "EO: Incorrect file length";
		case otaErr_ERASE_FAILED:
			return "EO: Erase failed";
		case otaErr_PROGRAM_FAILED:
			return "EO: Programming failed";
		case otaErr_TIMEOUT:
			return "EO: Timeout";
		case otaErr_VERIFY_CHECKSUM:
			return "EO: Checksum verification failed";
	}
	return "EO: Unknown OTA error";
}

static void ota_send_to_ws(const char *str)
{
	httpd_ws_frame_t ws_pkt;
	memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
	ws_pkt.payload = (uint8_t*)str;
	ws_pkt.len = strlen(str);
	ws_pkt.type = HTTPD_WS_TYPE_TEXT;

	for(size_t idx = 0; idx < sizeof(ota_wsfds)/sizeof(ota_wsfds[0]); ++idx) {
		if (!ota_wsfds[idx]) continue;
		httpd_ws_send_frame_async(ota_httpServer, ota_wsfds[idx], &ws_pkt);
	}
}

static int ota_exit_error(int err)
{
	if (ota_buffer) {
		free(ota_buffer);
		ota_buffer = NULL;
	}
	ota_partition = NULL;
	memset(&ota_handle, 0, sizeof(esp_ota_handle_t));
	ESP_LOGE(TAG, "%s", ota_err_str(err));
	ota_send_to_ws(ota_err_str(err));
	return err;
}

static int ota_perform(const char *b, size_t sz)
{
	esp_err_t err;

	if (!b && sz) return ota_exit_error(-1);
	if (ota_reboot_sheduled) return ota_exit_error(-4);

	if (!ota_buffer) {
		/* Begin the OTA process */

		if (!sz) return ota_exit_error(-2);

		ESP_LOGI(TAG, "OTA begin");
		ota_send_to_ws("IO: OTA begin");

		ota_error_buf[0] = '\0';

		/* Allocate data buffer */
		ota_buffer = (char*)malloc(OTA_DATA_BUFFER_SIZE);
		if (!ota_buffer) return ota_exit_error(-3);
		ota_buffer_pos = 0;
		ota_image_len = 0;

		/* Find update partition */
		ota_partition = esp_ota_get_next_update_partition(NULL);
		if (ota_partition == NULL) return ota_exit_error(-5);

		snprintf(ota_msg_buf, sizeof(ota_msg_buf),
				"IO: Writing to partition subtype %d at offset 0x%x",
				ota_partition->subtype, ota_partition->address
				);

				ESP_LOGI(TAG, "%s", ota_msg_buf);
				ota_send_to_ws(ota_msg_buf);

		err = esp_ota_begin(ota_partition, OTA_WITH_SEQUENTIAL_WRITES,
				&ota_handle);
		if (err != ESP_OK) {
			snprintf(ota_error_buf, sizeof(ota_error_buf),
					"EO: esp_ota_begin() failed (%s)", esp_err_to_name(err));
			return ota_exit_error(-6);
		}
	}

	size_t leftsz = sz;
	while (leftsz) {
		size_t cpsz = MIN(leftsz, (OTA_DATA_BUFFER_SIZE - ota_buffer_pos));
		memcpy(&ota_buffer[ota_buffer_pos], b, cpsz);
		ota_buffer_pos += cpsz;
		b += cpsz;
		leftsz -= cpsz;

		if (ota_buffer_pos == OTA_DATA_BUFFER_SIZE) {
			/* Buffer is full, write data */
			err = esp_ota_write(ota_handle, ota_buffer, OTA_DATA_BUFFER_SIZE);
			if (err != ESP_OK) {
				esp_ota_abort(ota_handle);
				snprintf(ota_error_buf, sizeof(ota_error_buf),
						"EO: esp_ota_write() failed 0x%x (%s)", err, esp_err_to_name(err));
				return ota_exit_error(-7);
			}
			ota_image_len += OTA_DATA_BUFFER_SIZE;
			ota_buffer_pos = 0;
			if (!(ota_image_len & 0x7FFF /* 32k */)) {
				snprintf(ota_msg_buf, sizeof(ota_msg_buf),
						"IO: Written image length %zu", ota_image_len
						);
				ESP_LOGI(TAG, "%s", ota_msg_buf);
				ota_send_to_ws(ota_msg_buf);
			}
		}
	}

	if (!sz && ota_buffer_pos) {
		/* Write the rest of the data */
		err = esp_ota_write(ota_handle, ota_buffer, ota_buffer_pos);
		if (err != ESP_OK) {
			esp_ota_abort(ota_handle);
			snprintf(ota_error_buf, sizeof(ota_error_buf),
					"EO: esp_ota_write() failed 0x%x (%s)", err, esp_err_to_name(err));
			return ota_exit_error(-7);
		}
		ota_image_len += ota_buffer_pos;
		ota_buffer_pos = 0;
		snprintf(ota_msg_buf, sizeof(ota_msg_buf),
				"IO: Written image length %zu (end of file)", ota_image_len
				);
		ESP_LOGI(TAG, "%s", ota_msg_buf);
		ota_send_to_ws(ota_msg_buf);
	}

	if (!sz) {
		/* Image if fully written, finalize */
		err = esp_ota_end(ota_handle);
		if (err != ESP_OK) {
			snprintf(ota_error_buf, sizeof(ota_error_buf),
					"EO: esp_ota_end() failed 0x%x (%s)", err, esp_err_to_name(err));
			return ota_exit_error(-8);
		}
		err = esp_ota_set_boot_partition(ota_partition);
		if (err != ESP_OK) {
			snprintf(ota_error_buf, sizeof(ota_error_buf),
					"EO: esp_ota_set_boot_partition() failed 0x%x (%s)", err, esp_err_to_name(err));
			return ota_exit_error(-9);
		}
		free(ota_buffer);
		ESP_LOGI(TAG, "OTA end");
		ota_send_to_ws("IO: OTA end");
		ota_send_to_ws("IO: Rebooting...");
		ota_do_reboot();
	}

	return 0;
}

static int PU_ota_exit_error(enum ota_err_t err)
{
	if (PU_ota_buffer) {
		free(PU_ota_buffer);
		PU_ota_buffer = NULL;
	}
	ota_send_to_ws(ota_err_str(err));
	plc_TX_mode = plcMode_NORMAL;
	return err;
}

static void boot_make_cmd(uint16_t cmd, uint16_t param, enum plc_boot_target_t target)
{
	memset(plc_TX_boot_data, 0, 58);
	plc_TX_boot_data[0] = (cmd >> 0) & 0xFF;
	plc_TX_boot_data[1] = (cmd >> 8) & 0xFF;
	plc_TX_boot_data[2] = (param >> 0) & 0xFF;
	plc_TX_boot_data[3] = (param >> 8) & 0xFF;
	plc_set_target(plc_TX_boot_data, target);
}

static void boot_send_cmd()
{
	uint32_t crc = crc32_calc(0, plc_TX_boot_data, 52);
	plc_TX_boot_data[52] = (crc >> 24) & 0xFF;
	plc_TX_boot_data[53] = (crc >> 16) & 0xFF;
	plc_TX_boot_data[54] = (crc >> 8) & 0xFF;
	plc_TX_boot_data[55] = (crc >> 0) & 0xFF;
	plc_TX_boot_new_data = true;
}

static bool boot_wait_data(uint16_t *res, uint16_t cmd)
{
	int ticks = 0;
	while (plc_RX_boot_Cmd != cmd) {
		if (ticks > OTA_PU_TIMEOUT) return false;
		vTaskDelay(1);
		ticks++;
	}
	if (res) *res = plc_RX_boot_Ret;
	return true;
}

static bool boot_send_cmd_wait_data(uint16_t* res, uint16_t cmd, uint16_t param, enum plc_boot_target_t target)
{
	boot_make_cmd(cmd, param, target);
	boot_send_cmd();
	return boot_wait_data(res, cmd);
}

static bool get_boot_ver_and_id(enum plc_boot_target_t target, uint16_t* ver, uint32_t* id)
{
	uint16_t res;
	if (!boot_send_cmd_wait_data(&res, 0x69, 0, target)) return false;
	snprintf(ota_msg_buf, sizeof(ota_msg_buf),
			"IO: %s boot version %02x.%02x", ((target == plcBoot_INV) ? "INV" : "LCC"),
			(int)(res >> 8), (int)(res & 0xFF)
			);
	ota_send_to_ws(ota_msg_buf);
	if (ver) *ver = res;
	if (!boot_send_cmd_wait_data(&res, 0x49, 0, target)) return false;
	uint32_t id_ = res;
	if (!boot_send_cmd_wait_data(&res, 0x8049, 1, target)) return false;
	id_ |= (uint32_t)res << 16;
	snprintf(ota_msg_buf, sizeof(ota_msg_buf),
			"IO: %s boot ID %02x%02x%02x%02x", ((target == plcBoot_INV) ? "INV" : "LCC"),
			(int)((id_ >> 0) & 0xff), (int)((id_ >> 8) & 0xff), (int)((id_ >> 16) & 0xff), (int)((id_ >> 24) & 0xff)
			);
	ota_send_to_ws(ota_msg_buf);
	if (id) *id = id_;
	return true;
}

static int PU_ota_perform(const char *b, size_t sz)
{
	static size_t size_last_reported = 0;
	if (!b && sz) return PU_ota_exit_error(otaErr_INCORRECT_PARAM);

	if (!PU_ota_buffer) {
		/* Begin the OTA process */
		size_last_reported = 0;

		if (!sz) return PU_ota_exit_error(otaErr_ZERO_SZ);

		ESP_LOGI(TAG, "PU OTA begin");
		ota_send_to_ws("IO: PU OTA begin");

		ota_error_buf[0] = '\0';

		/* Allocate data buffer */
		PU_ota_buffer = (char*)malloc(OTA_PU_BUFFER_SIZE);
		if (!PU_ota_buffer) return PU_ota_exit_error(otaErr_NO_MEM);
		ota_buffer_pos = 0;
		ota_image_len = 0;
	}

	if (sz) {
		/* Read data to buffer */
		size_t cpsz = MIN(sz, (OTA_PU_BUFFER_SIZE - ota_buffer_pos));
		if (cpsz != sz) return PU_ota_exit_error(otaErr_FILE_TOO_LARGE);
		memcpy(&PU_ota_buffer[ota_buffer_pos], b, sz);
		ota_buffer_pos += sz;
		if ((ota_buffer_pos - size_last_reported) > 0x1000) {
			snprintf(ota_msg_buf, sizeof(ota_msg_buf),
					"IO: Read image length %zu", ota_buffer_pos
					);
			ESP_LOGI(TAG, "%s", ota_msg_buf);
			ota_send_to_ws(ota_msg_buf);
			size_last_reported = ota_buffer_pos;
		}
		/* Wait till all is in buffer */
		return 0;
	}

	snprintf(ota_msg_buf, sizeof(ota_msg_buf),
			"IO: Read image length %zu", ota_buffer_pos
			);
	ESP_LOGI(TAG, "%s", ota_msg_buf);
	ota_send_to_ws(ota_msg_buf);
	/* Switching to boot mode */
	ota_send_to_ws("IO: PU OTA switching to boot mode");
	/* TODO: switch off inverter, disconnect from power line */
	boot_make_cmd(0, 0, plcBoot_INV);
	boot_send_cmd();
	plc_TX_mode = plcMode_BOOT;
	int ticks = 0;
	while (plc_RX_mode != plcMode_BOOT) {
		if (ticks > (OTA_PU_TIMEOUT * 4)) return PU_ota_exit_error(otaErr_TIMEOUT);
		vTaskDelay(1);
		ticks++;
	}
	ota_send_to_ws("IO: PU OTA in boot mode");
	uint16_t inv_boot_ver, lcc_boot_ver;
	uint32_t inv_boot_ID, lcc_boot_ID;
	if (!get_boot_ver_and_id(plcBoot_INV, &inv_boot_ver, &inv_boot_ID)) return PU_ota_exit_error(otaErr_TIMEOUT);
	if (!boot_send_cmd_wait_data(NULL, 0, 0, plcBoot_INV)) return PU_ota_exit_error(otaErr_TIMEOUT);
	if (!get_boot_ver_and_id(plcBoot_LCC, &lcc_boot_ver, &lcc_boot_ID)) return PU_ota_exit_error(otaErr_TIMEOUT);
	if (!boot_send_cmd_wait_data(NULL, 0, 0, plcBoot_LCC)) return PU_ota_exit_error(otaErr_TIMEOUT);
	uint32_t file_id = PU_ota_buffer[0] | (PU_ota_buffer[1] << 8) | (PU_ota_buffer[2] << 16) | (PU_ota_buffer[3] << 24);
	snprintf(ota_msg_buf, sizeof(ota_msg_buf),
			"IO: File ID %02x%02x%02x%02x",
			(int)((file_id >> 0) & 0xff), (int)((file_id >> 8) & 0xff), (int)((file_id >> 16) & 0xff), (int)((file_id >> 24) & 0xff)
			);
	ota_send_to_ws(ota_msg_buf);
	enum plc_boot_target_t target;
	if (file_id == inv_boot_ID) {
		target = plcBoot_INV;
		ota_send_to_ws("IO: PU OTA INV firmware detected");
	} else if (file_id == lcc_boot_ID) {
		target = plcBoot_LCC;
		ota_send_to_ws("IO: PU OTA LCC firmware detected");
	} else {
		return PU_ota_exit_error(otaErr_FILE_INCORRECT);
	}
	uint16_t len = PU_ota_buffer[42] | (PU_ota_buffer[43] << 8);
	if ((size_t)len * 16 + 32 != ota_buffer_pos) {
		return PU_ota_exit_error(otaErr_FILE_LEN_INCORRECT);
	}
	uint16_t res;
	if (!boot_send_cmd_wait_data(&res, 0x61, len - 1, target)) return PU_ota_exit_error(otaErr_TIMEOUT);
	uint16_t last_img_block = res;
	if (!boot_send_cmd_wait_data(&res, 0x68, 0, target)) return PU_ota_exit_error(otaErr_TIMEOUT);
	uint16_t hash_addr = res;
	if (!boot_send_cmd_wait_data(&res, 0x61, hash_addr, target)) return PU_ota_exit_error(otaErr_TIMEOUT);
	uint16_t hash_block = res;
	/* Erasing */
	uint16_t cmd = 0x8845;
	for (uint16_t b = 0; b <= last_img_block + 1; ++b) {
		if (b == last_img_block + 1) {
			if (hash_block <= last_img_block) break;
			b = hash_block;
		}
		cmd ^= 0x8000;
		snprintf(ota_msg_buf, sizeof(ota_msg_buf), "IO: Erasing block %i", (int)b);
		ota_send_to_ws(ota_msg_buf);
		if (!boot_send_cmd_wait_data(&res, cmd, b, target)) return PU_ota_exit_error(otaErr_TIMEOUT);
		if (res != 0 && res != 0x8000) {
			snprintf(ota_msg_buf, sizeof(ota_msg_buf), "IO: Returned %hu", res);
			ota_send_to_ws(ota_msg_buf);
			return PU_ota_exit_error(otaErr_ERASE_FAILED);
		}
	}
	cmd = 0x8850;
	for (uint16_t a = 0; a < len; a += 3) {
		int n = ((len - a) > 2) ? 3 : (len-a);
		bool skip = true;
		for (size_t ba = (size_t)a * 16 + 32; ba < (size_t)(a + n) * 16 + 32; ++ba) {
			if (PU_ota_buffer[ba] != 0xFF) {
				skip = false;
				break;
			}
		}
		if (skip) {
			snprintf(ota_msg_buf, sizeof(ota_msg_buf), "IO: Skipping %i chunks at address 0x%04hx", n, a);
			ota_send_to_ws(ota_msg_buf);
			continue;
		};
		snprintf(ota_msg_buf, sizeof(ota_msg_buf), "IO: Programming %i chunks at address 0x%04hx (%i/%i)", n, a, (int)a / 3 + 1, (int)len / 3);
		ota_send_to_ws(ota_msg_buf);
		cmd ^= 0x8000;
		cmd = (cmd & 0xFCFF) | (n << 8);
		boot_make_cmd(cmd, a, target);
		memcpy(&plc_TX_boot_data[4], &PU_ota_buffer[(size_t)a * 16 + 32], n * 16);
		boot_send_cmd();
		if (!boot_wait_data(&res, cmd)) return PU_ota_exit_error(otaErr_TIMEOUT);
		if (res != 0 && res != 0x8000) {
			snprintf(ota_msg_buf, sizeof(ota_msg_buf), "IO: Returned %hu", res);
			ota_send_to_ws(ota_msg_buf);
			return PU_ota_exit_error(otaErr_PROGRAM_FAILED);
		}
	}

	snprintf(ota_msg_buf, sizeof(ota_msg_buf), "IO: Programming hash at address 0x%04hx", hash_addr);
	ota_send_to_ws(ota_msg_buf);
	cmd ^= 0x8000;
	cmd = (cmd & 0xFCFF) | (1 << 8);
	boot_make_cmd(cmd, hash_addr, target);
	memcpy(&plc_TX_boot_data[4], &PU_ota_buffer[16], 16);
	boot_send_cmd();
	if (!boot_wait_data(&res, cmd)) return PU_ota_exit_error(otaErr_TIMEOUT);
	if (res != 0 && res != 0x8000) {
		snprintf(ota_msg_buf, sizeof(ota_msg_buf), "IO: Returned %hu", res);
		ota_send_to_ws(ota_msg_buf);
		return PU_ota_exit_error(otaErr_PROGRAM_FAILED);
	}

	if (PU_ota_buffer) {
		free(PU_ota_buffer);
		PU_ota_buffer = NULL;
	}

	/* Verify */
	if (!boot_send_cmd_wait_data(&res, 0x56, 0, target)) return PU_ota_exit_error(otaErr_TIMEOUT);
	if (res) {
		return PU_ota_exit_error(otaErr_VERIFY_CHECKSUM);
	}

	ota_send_to_ws("IO: Programming finished, exiting boot mode");
	boot_make_cmd(0x6a, 0, plcBoot_INV);
	boot_send_cmd();
	ticks = 0;
	while (plc_RX_mode == plcMode_BOOT) {
		if (ticks > (OTA_PU_TIMEOUT * 4)) return PU_ota_exit_error(otaErr_TIMEOUT);
		vTaskDelay(1);
		ticks++;
	}
	plc_TX_mode = plcMode_NORMAL;
	ota_send_to_ws("IO: Done");
	return 0;
}

static esp_err_t ota_uri_fw_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGhttp, "executing %s() on CORE%i", __func__, xPortGetCoreID());
	extern const char start_fw_html[] asm("_binary_fw_html_start");
	extern const char end_fw_html[] asm("_binary_fw_html_end");
	httpd_resp_send(req, start_fw_html, end_fw_html - start_fw_html - 1 /* omit '\0' */);
	return ESP_OK;
}

static const httpd_uri_t ota_uri_fw = {
	.uri = "/fw",
	.method = HTTP_GET,
	.handler = ota_uri_fw_handler,
	.user_ctx = NULL
};

static esp_err_t ota_uri_fw_js_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGhttp, "executing %s() on CORE%i", __func__, xPortGetCoreID());
	extern const char start_fw_js[] asm("_binary_fw_js_start");
	extern const char end_fw_js[] asm("_binary_fw_js_end");
	httpd_resp_set_type(req, "text/javascript");
	httpd_resp_send(req, start_fw_js, end_fw_js - start_fw_js - 1 /* omit '\0' */);
	return ESP_OK;
}

static const httpd_uri_t ota_uri_fw_js = {
	.uri = "/fw.js",
	.method = HTTP_GET,
	.handler = ota_uri_fw_js_handler,
	.user_ctx = NULL
};

static int my_strncasecmp(const char *s1, const char *s2, size_t sz)
{
    const unsigned char *us1 = (const u_char *)s1,
                        *us2 = (const u_char *)s2;
	size_t i = 0;
	if (!sz) return 0;

    while (tolower(*us1) == tolower(*us2++))
        if (*us1++ == '\0' || ++i == sz)
            return 0;
    return (tolower(*us1) - tolower(*--us2));
}

static const char * match_header(const char *s, size_t sz, const char *type, const char *value)
{
	if (!s || !sz || !type) return NULL;
	size_t tsz = strlen(type);
	if (sz < tsz + 1) return NULL;
	if (my_strncasecmp(s, type, tsz)) return NULL;
	/* TODO: skip spaces before ':' */
	if (s[tsz] != ':') return NULL;
	/* TODO: skip spaces after ':' and return beginning of value */
	if (!value) return &s[tsz + 1];
	s = &s[tsz + 1];
	sz -= tsz + 1;
	while ((*s == ' ' || *s == '\t') && sz) {
		++s;
		--sz;
	}
	size_t vsz = strlen(value);
	if (sz < vsz) return NULL;
	if (my_strncasecmp(s, value, vsz)) return NULL;
	if (sz == vsz) return &s[vsz];
	if (s[vsz] != ';') return NULL;
	s = &s[vsz + 1];
	sz -= vsz + 1;
	while ((*s == ' ' || *s == '\t') && sz) {
		++s;
		--sz;
	}
	return s;
}

const char *next_attribute(const char *s, size_t sz, size_t *nsz)
{
	const char *ret = s;
	while (*ret != '=' && sz) {
		++ret;
		--sz;
	}
	if (!sz) return NULL;
	++ret;
	--sz;
	if (!sz) return NULL;
	int quoted = (*ret == '"') ? 1 : 0;
	if (quoted) {
		while (*ret != '"' && sz) {
			++ret;
			--sz;
		}
		if (!sz) return NULL;
		++ret;
		--sz;
		if (!sz) return NULL;
	}
	while (*ret != ';' && sz) {
		++ret;
		--sz;
	}
	if (!sz) return NULL;
	++ret;
	--sz;
	if (!sz) return NULL;
	while ((*ret == ' ' || *ret == '\t') && sz) {
		++ret;
		--sz;
	}
	if (!sz) return NULL;
	if (nsz) *nsz = sz;
	return ret;
}

static int match_attribute(const char *s, size_t sz, const char *name, const char *value)
{
	if (!s || !sz || !name) return 0;
	size_t nsz = strlen(name);
	size_t vsz = 0;
	if (value) vsz = strlen(value);
	while (s && sz >= (nsz + vsz + 1)) {
		if (
				my_strncasecmp(s, name, nsz)
				|| s[nsz] != '='
				) {
			s = next_attribute(s, sz, &sz);
			continue;
		}
		if (!value) return 1;
		const char *v = &s[nsz + 1];
		size_t svsz = sz - nsz - 1;
		int quoted = (*v == '"') ? 1 : 0;
		if (quoted) {
			++v;
			--svsz;
		}
		if (svsz < vsz) return 0;
		if (strncmp(v, value, vsz)) return 0;
		if (quoted && svsz > vsz && v[vsz] == '"') return 1;
		if (!quoted && (svsz == vsz || v[vsz] == ';')) return 1;
		break;
	}
	return 0;
}

static size_t dump_data(const char *b, size_t cnt, int flag, const char *type)
{
	static int begin = 1;
	static size_t pos = 0;
	if (flag & 0x1) {
		begin = 1;
		pos = 0;
	}
	int end = flag & 0x2;
	if (begin) {
		printf("============= Dump data begin ==============");
		if (type) printf(" (%s)", type);
		printf("\n");
		begin = 0;
	}
	size_t i;
	for (i = 0; i < cnt; i += 16) {
		if (cnt - i < 16 && !end) break;
		printf("%08lx: ", (unsigned long)pos);
		for (size_t j = i; j < i + 16; ++j) {
			if (j >= cnt) {
				printf("  ");
			} else {
				printf("%02x", (int)b[j]);
			}
			if ((j - i) & 1) printf(" ");
		}
		printf(" ");
		for (size_t j = i; j < i + 16; ++j) {
			if (j >= cnt) {
				printf(" ");
			} else if (b[j] >= 0x20 && b[j] < 0x7f) {
				printf("%c", b[j]);
			} else {
				printf(".");
			}
		}
		printf("\n");
		pos += 16;
	}
	if (end) {
		printf("============== Dump data end ===============");
		if (type) printf(" (%s)", type);
		printf("\n");
	}
	return (i > cnt) ? cnt : i;
}

static size_t handle_form_data(const char *b, size_t cnt, int flag)
{
	static int headers = 1;
	static int data_begin = 1;
	static int fwdata_flags = 0;
	static int already_failed;
	if (flag & 0x1) {
		/* Begin */
		already_failed = 0;
		headers = 1;
		data_begin = 1;
		fwdata_flags = 0;
	}
	int end = flag & 0x2;
	static const char * crlf = "\r\n";
	size_t i = 0;
	if (headers) {
		int crlf_match = 1;
		while (cnt - i >= 2 && (crlf_match = strncmp(&b[i], crlf, 2)) != 0) {
			const char *p;
			if ((p = strstr(&b[i], crlf)) != NULL && (p - &b[i] + 2) <= (cnt - i)) {
				/* here we have a header beginning at &b[i], with length (p - &b[i]) */
				snprintf(ota_msg_buf, sizeof(ota_msg_buf),
						"DH: %.*s", (int)(p - &b[i]), &b[i]);
				ota_send_to_ws(ota_msg_buf);
				ESP_LOGI(TAGhttp, "%s", ota_msg_buf);
				const char *he;
				if ((he = match_header(&b[i], p - &b[i], "Content-Disposition", "form-data")) != NULL
						&& match_attribute(he, p - he, "name", "firmware-file")
						) {
					ota_send_to_ws("IH: Matched Content-Disposition CU");
					ESP_LOGI(TAGhttp, "Matched Content-Disposition CU");
					fwdata_flags |= (1 << 1);
				}
				if ((he = match_header(&b[i], p - &b[i], "Content-Disposition", "form-data")) != NULL
						&& match_attribute(he, p - he, "name", "firmware-file-pu")
						) {
					ota_send_to_ws("IH: Matched Content-Disposition PU");
					ESP_LOGI(TAGhttp, "Matched Content-Disposition PU");
					fwdata_flags |= (1 << 2);
				}
				if ((he = match_header(&b[i], p - &b[i], "Content-Type", "application/octet-stream")) != NULL) {
					ota_send_to_ws("IH: Matched Content-Type");
					ESP_LOGI(TAGhttp, "Matched Content-Type");
					fwdata_flags |= (1 << 0);
				}
				i += p - &b[i] + 2;
			} else {
				if (!end) return i;
				else break;
			}
		}
		if (!crlf_match) {
			i += 2;
			headers = 0;
		} else {
			if (!end) return i;
		}
	}
	size_t ret = i;
	if (fwdata_flags == 0x3) {
		/* OTA CU */
		ret = cnt; // ota_perform will eat all data given
		if (!already_failed && cnt - i) {
			if (ota_perform(&b[i], cnt - i)) already_failed = 1;
		}
		if (!already_failed && end) {
			ota_perform(NULL, 0);
		}
	} else if (fwdata_flags == 0x5) {
		/* OTA PU */
		ret = cnt;
		if (!already_failed && cnt - i) {
			if (PU_ota_perform(&b[i], cnt - i)) already_failed = 1;
		}
		if (!already_failed && end) {
			PU_ota_perform(NULL, 0);
		}
	} else {
		//ret += dump_data(&b[i], cnt - i, (data_begin ? 1 : 0) | (end ? (1 << 1) : 0), "other");
		ret = cnt;
	}

	data_begin = 0;
	
	return ret;
}

/*
 * https://datatracker.ietf.org/doc/html/rfc2045
 * https://datatracker.ietf.org/doc/html/rfc2046
 * https://datatracker.ietf.org/doc/html/rfc2047
 * https://datatracker.ietf.org/doc/html/rfc822
 */
static int parse_multipart_upd(httpd_req_t *req, char *ct)
{
    char buf[256];
	int bufsz = 0;
    int remaining = req->content_len;
	int ret;
	static const char *bstr = "boundary=";
	char *boundary;
	char *bp = strstr(ct, bstr);
	if (!bp) return -2;
	bp += strlen(bstr);
	static const char *bchrs = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'()+_,-./:=? ";
	if (*bp == '"') bp += 1;
	size_t bsz = strspn(bp, bchrs);
	if (bsz > 70) bsz = 70;
	while (bsz && bp[bsz - 1] == ' ') bsz--;
	if (!bsz) return -3;
	boundary = bp - 2;
	boundary[0] = '-';
	boundary[1] = '-';
	bsz += 2;
	boundary[bsz] = '\0';
	ESP_LOGI(TAGhttp, "Boundary: '%s'", boundary);

	enum {
		stPREAMBLE = 0,
		stPART,
		stEPILOGUE
	} state = stPREAMBLE;

	//ESP_LOGI(TAGhttp, "============= PREAMBLE =============");

	int part_begin = 1;

	while (remaining > 0) {
		/* Read the data for the request */
		if ((ret = httpd_req_recv(req, &buf[bufsz], MIN(remaining, (sizeof(buf) - bufsz - 1)))) <= 0) {
			if (ret == HTTPD_SOCK_ERR_TIMEOUT) continue;
			return -1;
		}
		remaining -= ret;
		bufsz += ret;
		if (bufsz < sizeof(buf)) buf[bufsz] = '\0';
		bp = buf;
		for (;;) {
			if (state == stPREAMBLE || state == stPART) {
				while ((bp = memchr(bp, '-', bufsz - (bp - buf))) != 0
						&& (bufsz - (bp - buf)) >= bsz
						&& strncmp(bp, boundary, bsz)
						) {
					++bp;
				}
			} else {
				bp = NULL;
			}

			size_t dump;
			if (!bp || (bufsz - (bp - buf)) < bsz) {
				/* Boundary not found */
				dump = (remaining && bufsz > 128) ? bufsz - 128 : bufsz;
				if (state == stPART) dump = handle_form_data(buf, dump, part_begin);
				part_begin = 0;
				if (dump < bufsz) {
					memmove(buf, buf + dump, bufsz - dump);
					bufsz -= dump;
				}
				break;
			} else {
				/* Boundary found */
				dump = bp - buf;
				if (state == stPART) handle_form_data(buf, (dump > 2) ? dump - 2 : dump, part_begin | 2);
				if (bufsz >= dump + bsz + 2 && bp[bsz] == '-' && bp[bsz + 1] == '-') {
					/* Close delimitier */
					//ESP_LOGI(TAGhttp, "============= EPILOGUE =============");
					state = stEPILOGUE;
					memmove(buf, buf + dump + bsz + 2, bufsz - dump - bsz - 2);
					bufsz -= dump + bsz + 2;
					bp = buf;
					/* FIXME: here we should also remove transport padding and CRLF
					 * which are not part of the epilogue
					 */
					if (remaining) break;
					else           continue;
				} else {
					char *ep = bp + bsz;
					while (ep < buf + bufsz) {
						if (*ep == ' ' || *ep == '\t') ++ep;
						else if (*ep == '\r' && ep < (buf + bufsz - 2) && ep[1] == '\n') {
							ep = ep + 2;
							break;
						} else {
							return -3;
						}
					}
					if (ep < buf + bufsz) {
						memmove(buf, ep, bufsz - (ep - buf));
						bufsz -= ep - buf;
						state = stPART;
						ESP_LOGI(TAGhttp, "=============== PART ===============");
						part_begin = 1;
						bp = buf;
						if (remaining) break;
						else           continue;
					} else {
						memmove(buf, buf + dump, bufsz - dump);
						bufsz -= dump;
						bp = buf;
						if (remaining) break;
						else           continue;
					}
				}
			}
		}
	}

	return 0;
}

static esp_err_t ota_uri_upd_handler(httpd_req_t *req)
{
    ESP_LOGD(TAGhttp, "executing %s() on CORE%i", __func__, xPortGetCoreID());
    int ret;

    char*  ct;
    size_t ct_len = httpd_req_get_hdr_value_len(req, "Content-Type") + 1;
    if (ct_len < 2) {
		ota_send_to_ws("ER: Content-Type header not found in request");
		ESP_LOGW(TAGhttp, "Content-Type header not found in request");
		httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Content-Type not found");
		return ESP_OK;
	}

	ct = malloc(ct_len);
	/* Copy null terminated value string into buffer */
	if (httpd_req_get_hdr_value_str(req, "Content-Type", ct, ct_len) != ESP_OK) {
		ota_send_to_ws("ER: Unable to copy Content-Type header value");
		ESP_LOGW(TAGhttp, "Unable to copy Content-Type header value");
		httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Unable to copy Content-Type header value");
		free(ct);
		return ESP_OK;
	}

	ESP_LOGI(TAGhttp, "Content-Type: '%s'", ct);
	static const char *ct_mp = "multipart/form-data";
	if (strlen(ct) > strlen(ct_mp) && !strncmp(ct, ct_mp, strlen(ct_mp))) {
		if ((ret = parse_multipart_upd(req, ct))) {
			if (ret == -1) {
				return ESP_FAIL;
			} else {
				ota_send_to_ws("ER: Unable to parse multipart request");
				ESP_LOGW(TAGhttp, "Unable to parse multipart request");
				httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Unable to parse multipart request");
				free(ct);
				return ESP_OK;
			}
		}
	} else {
		ota_send_to_ws("ER: Content-Type not supported");
		ESP_LOGW(TAGhttp, "Content-Type not supported");
		httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Content-Type not supported");
		free(ct);
		return ESP_OK;
	}

	free(ct);

    httpd_resp_send(req, "Update complete", HTTPD_RESP_USE_STRLEN);

    return ESP_OK;
}

static const httpd_uri_t ota_uri_upd = {
	.uri = "/upd",
	.method = HTTP_POST,
	.handler = ota_uri_upd_handler,
	.user_ctx = NULL
};

/* https://github.com/espressif/esp-idf/issues/5406 */

#if 0
/*
 * Structure holding server handle
 * and internal socket fd in order
 * to use out of request send
 */
struct async_resp_arg {
    httpd_handle_t hd;
    int fd;
};

/*
 * async send function, which we put into the httpd work queue
 */
static void ws_async_send(void *arg)
{
    static const char * data = "Async data";
    struct async_resp_arg *resp_arg = arg;
    httpd_handle_t hd = resp_arg->hd;
    int fd = resp_arg->fd;
    httpd_ws_frame_t ws_pkt;
    memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
    ws_pkt.payload = (uint8_t*)data;
    ws_pkt.len = strlen(data);
    ws_pkt.type = HTTPD_WS_TYPE_TEXT;

    httpd_ws_send_frame_async(hd, fd, &ws_pkt);
    free(resp_arg);
}

static esp_err_t trigger_async_send(httpd_handle_t handle, httpd_req_t *req)
{
    struct async_resp_arg *resp_arg = malloc(sizeof(struct async_resp_arg));
    resp_arg->hd = req->handle;
    resp_arg->fd = httpd_req_to_sockfd(req);
    return httpd_queue_work(handle, ws_async_send, resp_arg);
}
#endif

static esp_err_t ota_uri_fwws_handler(httpd_req_t *req)
{
	ESP_LOGD(TAGws, "executing %s() on CORE%i", __func__, xPortGetCoreID());

	uint8_t buf[128] = { 0 };
	httpd_ws_frame_t ws_pkt;
	memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
	ws_pkt.payload = buf;
	ws_pkt.type = HTTPD_WS_TYPE_TEXT;
	esp_err_t ret = httpd_ws_recv_frame(req, &ws_pkt, 128);
	if (ret != ESP_OK) {
		ESP_LOGE(TAGws, "httpd_ws_recv_frame failed with %d", ret);
		return ret;
	}
	ESP_LOGI(TAGws, "Got packet with message: %s", ws_pkt.payload);
	ESP_LOGI(TAGws, "Packet type: %d", ws_pkt.type);

	if (ws_pkt.type == HTTPD_WS_TYPE_TEXT && !strcmp((char*)ws_pkt.payload, "hi")) {
		bool there = false;
		for(size_t idx = 0; idx < sizeof(ota_wsfds)/sizeof(ota_wsfds[0]); ++idx) {
			if (ota_wsfds[idx] == httpd_req_to_sockfd(req)) {
				there = true;
				break;
			}
		}

		if (!there) {
			for(size_t idx = 0; idx < sizeof(ota_wsfds)/sizeof(ota_wsfds[0]); ++idx) {
				if (ota_wsfds[idx] == 0) {
					ota_wsfds[idx] = httpd_req_to_sockfd(req);
					break;
				}
			}
		}
	}

	/*
	ret = httpd_ws_send_frame(req, &ws_pkt);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "httpd_ws_send_frame failed with %d", ret);
	}
	*/
	return ret;
}

static const httpd_uri_t ota_uri_fwws = {
	.uri = "/fw.ws",
	.method = HTTP_GET,
	.handler = ota_uri_fwws_handler,
	.user_ctx = NULL,
	.is_websocket = true
};

void ota_register_uris(httpd_handle_t server)
{
	ota_httpServer = server;
	httpd_register_uri_handler(server, &ota_uri_fw);
	httpd_register_uri_handler(server, &ota_uri_fw_js);
	httpd_register_uri_handler(server, &ota_uri_fwws);
	httpd_register_uri_handler(server, &ota_uri_upd);
}

void ota_on_fd_close(httpd_handle_t hd, int sockfd)
{
	ESP_LOGI(TAGws, "executing %s(%i) on CORE%i", __func__, sockfd, xPortGetCoreID());

	if (ota_prevCloseFunc) (*ota_prevCloseFunc)(hd, sockfd);
	for(size_t idx = 0; idx < sizeof(ota_wsfds)/sizeof(ota_wsfds[0]); ++idx) {
		if (ota_wsfds[idx] == sockfd) ota_wsfds[idx] = 0;
	}
}

void ota_register_handlers(httpd_config_t *cfg)
{
	ota_prevCloseFunc = cfg->close_fn;
	cfg->close_fn = ota_on_fd_close;
}

