var ctrlReq;
var statReq;
var sensReq;
var chart;

const relayStatus = document.getElementById('relayStatus');
const mosfetStatus = document.getElementById('mosfetStatus');
const puStatus = document.getElementById('puStatus');
const prioStatus = document.getElementById('prioStatus');
const fpStatus = document.getElementById('fpStatus');

const pwrData = document.getElementById('pwrData');
const tMaxPU = document.getElementById('tMaxPU');
const pvPwr = document.getElementById('pvPwr');
const invSP = document.getElementById('invSP');
const invStatus = document.getElementById('invStatus');
const plcErrRate = document.getElementById('plcErrRate');
const plcErrSeq = document.getElementById('plcErrSeq');
const plcNorepRate = document.getElementById('plcNorepRate');
const plcNorepSeq = document.getElementById('plcNorepSeq');
const st7540Status = document.getElementById('st7540Status');

document.getElementById('relayON').addEventListener('click', function(){makeCtrlRequest('?relay=ON')});
document.getElementById('relayOFF').addEventListener('click', function(){makeCtrlRequest('?relay=OFF')});
document.getElementById('mosfetON').addEventListener('click', function(){makeCtrlRequest('?mosfet=ON')});
document.getElementById('mosfetOFF').addEventListener('click', function(){makeCtrlRequest('?mosfet=OFF')});
document.getElementById('puON').addEventListener('click', function(){makeCtrlRequest('?pu=ON')});
document.getElementById('puOFF').addEventListener('click', function(){makeCtrlRequest('?pu=OFF')});
document.getElementById('prioINV').addEventListener('click', function(){makeCtrlRequest('?prio=INV')});
document.getElementById('prioHTR').addEventListener('click', function(){makeCtrlRequest('?prio=HTR')});
document.getElementById('fpON').addEventListener('click', function(){makeCtrlRequest('?fp=ON')});
document.getElementById('fpOFF').addEventListener('click', function(){makeCtrlRequest('?fp=OFF')});

function setAllStatus(str) {
	relayStatus.innerHTML = str;
	mosfetStatus.innerHTML = str;
	puStatus.innerHTML = str;
	prioStatus.innerHTML = str;
	fpStatus.innerHTML = str;
}

/* ctrl */
function makeCtrlRequest(rq) {
	ctrlReq = new XMLHttpRequest();
	ctrlReq.timeout = 500;

	if (!ctrlReq) {
		setAllStatus('noREQ');
		return false;
	}
	ctrlReq.onreadystatechange = ctrlReadyState;
	ctrlReq.ontimeout = ctrlTimeout;
	ctrlReq.open('GET', '/ctrl' + rq);
	ctrlReq.send();
}

function ctrlReadyState() {
	if (ctrlReq.readyState === XMLHttpRequest.DONE) {
		if (ctrlReq.status === 200) {
			let dataArray = ctrlReq.responseText.split('\n');
			relayStatus.innerHTML = dataArray[0];
			mosfetStatus.innerHTML = dataArray[1];
			puStatus.innerHTML = dataArray[2];
			prioStatus.innerHTML = dataArray[3];
			fpStatus.innerHTML = dataArray[4];
		} else {
			setAllStatus('noREP');
		}
	}
}

function ctrlTimeout() {
	setAllStatus('TIMEOUT');
	makeCtrlRequest('');
}

function undefData() {
	pwrData.innerHTML = 'n/a';
	tMaxPU.innerHTML = 'n/a';
	pvPwr.innerHTML = 'n/a';
	invSP.innerHTML = 'n/a';
	invStatus.innerHTML = 'n/a';
	plcErrRate.innerHTML = 'n/a';
	plcErrSeq.innerHTML = 'n/a';
	plcNorepRate.innerHTML = 'n/a';
	plcNorepSeq.innerHTML = 'n/a';
	st7540Status.innerHTML = 'n/a';
}

/* stat */
function makeStatRequest() {
	statReq = new XMLHttpRequest();
	statReq.timeout = 500;
	if (!statReq) {
		console.log('Unable to create XMLHTTP instance');
		undefData();
		setTimeout(makeStatRequest, 300);
		return false;
	}
	statReq.onreadystatechange = statReadyState;
	statReq.ontimeout = statTimeout;
	statReq.open('GET', '/stat', true);
	statReq.send();
}

function statReadyState() {
	if (statReq.readyState === XMLHttpRequest.DONE) {
		if (statReq.status === 200) {
			let dataArray = statReq.responseText.split('\n');
			pwrData.innerHTML = dataArray[0];
			tMaxPU.innerHTML = dataArray[1];
			pvPwr.innerHTML = dataArray[2];
			invSP.innerHTML = dataArray[3];
			invStatus.innerHTML = dataArray[4];
			plcErrRate.innerHTML = dataArray[5];
			plcErrSeq.innerHTML = dataArray[6];
			plcNorepRate.innerHTML = dataArray[7];
			plcNorepSeq.innerHTML = dataArray[8];
			st7540Status.innerHTML = dataArray[9];
			if (relayStatus.innerHTML === 'n/a') {
				makeCtrlRequest('');
			}
		} else {
			undefData();
			console.log('There was a problem with the request');
		}
		setTimeout(makeStatRequest, 300);
	}
}

function statTimeout() {
	undefData();
	setAllStatus('n/a');
	makeStatRequest();
}

/* sens */
function makeSensRequest() {
	sensReq = new XMLHttpRequest();
	sensReq.timeout = 500;
	if (!sensReq) {
		console.log('Unable to create XMLHTTP instance');
		document.getElementById('line-graph').innerHTML = 'noREQ';
		setTimeout(makeSensRequest, 300);
		return false;
	}
	sensReq.onreadystatechange = sensReadyState;
	sensReq.ontimeout = sensTimeout;
	sensReq.open('GET', '/sense', true);
	sensReq.send();
}

function sensReadyState() {
	if (sensReq.readyState === XMLHttpRequest.DONE) {
		if (sensReq.status === 200) {
			if ((typeof google === 'undefined')
				|| (typeof google.visualization === 'undefined')
				|| (typeof google.visualization.DataTable === 'undefined')
				|| (typeof google.visualization.LineChart === 'undefined')
			) {
				console.log('Google chart is not loaded yet');
				document.getElementById('line-graph').innerHTML = 'google.charts loading...'
				setTimeout(makeSensRequest, 1000);
				return;
			}
			let dataArray = sensReq.responseText.split(/\r?\n/).filter(element => element);
			var data = new google.visualization.DataTable();
			data.addColumn('number', 'Time, ms');
			data.addColumn('number', 'Voltage, V');
			data.addColumn('number', 'Current, A');
			for (let i = 0; i < dataArray.length; i++) {
				let arr = dataArray[i].split(/,\s*/).map(parseFloat);
				data.addRow(arr);
			}

			var options = {
				title: 'Line sense graph',
				// Gives each series an axis that matches the vAxes number below.
				series: {
					0: {targetAxisIndex: 1},
					1: {targetAxisIndex: 0}
				},
				vAxes: {
					// Adds titles to each axis.
					0: {title: 'Current, A'},
					1: {title: 'Voltage, V', gridlines: {count: 0}}
				},
				legend: { position: 'bottom' },
				enableInteractivity: false
			};

			if (chart === undefined || chart === null) {
				chart = new google.visualization.LineChart(document.getElementById('line-graph'));
			} else {
				chart.clearChart();
			}
			chart.draw(data, options);
		} else {
			document.getElementById('line-graph').innerHTML = 'noREP';
			console.log('There was a problem with the request');
		}
		setTimeout(makeSensRequest, 1000);
	}
}

function sensTimeout() {
	document.getElementById('line-graph').innerHTML = 'TIMEOUT';
	makeSensRequest();
}

makeCtrlRequest('');
makeStatRequest();
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(makeSensRequest());

