var gateway = `ws://${window.location.hostname}/fw.ws`;
var webSocket;
var updReq;
var fileSize = 0;

window.addEventListener('load', windowOnload);

function windowOnload(ev)
{
	document.getElementById('submitButton').addEventListener('click', sendData);
	document.getElementById('submitButtonPU').addEventListener('click', sendDataPU);
	initWebSocket();
}

function initWebSocket()
{
	console.log(`Trying to open WebSocket connection to ${gateway}`);
	webSocket = new WebSocket(gateway);
	if (!webSocket) {
		console.log('Unable to create WebSocket instance');
		appendLog('ES: Unable to create WebSocket instance');
		return false;
	}
	webSocket.onopen = webSocketOnOpen;
	webSocket.onclose = webSocketOnClose;
	webSocket.onmessage = webSocketOnMessage;
}

function webSocketOnOpen(ev)
{
	console.log('Websocket connection opened');
	appendLog('Websocket connection opened');
	webSocket.send('hi');
}

function webSocketOnClose(ev)
{
	console.log('Websocket connection closed');
	appendLog('Websocket connection closed');
	setTimeout(initWebSocket, 2000);
}

function webSocketOnMessage(ev)
{
	appendLog(ev.data);
}

function appendLog(msg)
{
	var log = document.getElementById('log');
	var isScrolledToBottom = (log.scrollHeight - log.clientHeight <= log.scrollTop + 1);
	var str = log.innerHTML;
	var date = new Date();
	if (str.length != 0) {
		str += "\n";
	}
	str += `[${date.toISOString()}] ${msg}`;
	log.innerHTML = str;
	if (isScrolledToBottom) {
		log.scrollTop = log.scrollHeight;
	}
}

/* upd */
function sendData()
{
	var files = document.getElementById('firmware-file').files;
	if (files.length <= 0) {
		appendLog('No file selected');
		console.log('No file selected');
		return false;
	}
	var formData = new FormData();
	if (!formData) {
		console.log('Unable to create FormData instance');
		appendLog('ES: Unable to create FormData instance');
		return false;
	}
	formData.append('firmware-file', files[0]);
	fileSize = files[0].size;
	appendLog(`IS: File size = ${fileSize}`);

	updReq = new XMLHttpRequest();
	if (!updReq) {
		console.log('Unable to create XMLHTTP instance');
		appendLog('ES: Unable to create XMLHTTP instance');
		return false;
	}
	updReq.open('POST', '/upd');
	updReq.send(formData);
}

function sendDataPU()
{
	var files = document.getElementById('firmware-file-pu').files;
	if (files.length <= 0) {
		appendLog('No file selected');
		console.log('No file selected');
		return false;
	}
	var formData = new FormData();
	if (!formData) {
		console.log('Unable to create FormData instance');
		appendLog('ES: Unable to create FormData instance');
		return false;
	}
	formData.append('firmware-file-pu', files[0]);
	fileSize = files[0].size;
	appendLog(`IS: File size = ${fileSize}`);

	updReq = new XMLHttpRequest();
	if (!updReq) {
		console.log('Unable to create XMLHTTP instance');
		appendLog('ES: Unable to create XMLHTTP instance');
		return false;
	}
	updReq.open('POST', '/upd');
	updReq.send(formData);
}

// TODO: See also: https://webkit.org/blog/516/webkit-page-cache-ii-the-unload-event/
window.onbeforeunload = function() {
	if (webSocket  === undefined || webSocket === null) {
		return;
	}
    webSocket.onclose = function () {}; // disable onclose handler first
    webSocket.close();
};

