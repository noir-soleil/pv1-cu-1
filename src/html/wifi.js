var gateway = `ws://${window.location.hostname}/wifi.ws`;
var webSocket;
var updReq;
var fileSize = 0;

window.addEventListener('load', windowOnload);

function windowOnload(ev)
{
	initWebSocket();
}

function initWebSocket()
{
	console.log(`Trying to open WebSocket connection to ${gateway}`);
	webSocket = new WebSocket(gateway);
	if (!webSocket) {
		console.log('Unable to create WebSocket instance');
		return false;
	}
	webSocket.onopen = webSocketOnOpen;
	webSocket.onclose = webSocketOnClose;
	webSocket.onmessage = webSocketOnMessage;
}

function webSocketOnOpen(ev)
{
	console.log('Websocket connection opened');
	webSocket.send('scan');
}

function webSocketOnClose(ev)
{
	console.log('Websocket connection closed');
	setTimeout(initWebSocket, 2000);
}

function webSocketOnMessage(ev)
{
	updateList(ev.data);
}

function updateList(msg)
{
	document.getElementById('list').innerHTML = msg;
}

// TODO: See also: https://webkit.org/blog/516/webkit-page-cache-ii-the-unload-event/
window.onbeforeunload = function() {
	if (webSocket  === undefined || webSocket === null) {
		return;
	}
    webSocket.onclose = function () {}; // disable onclose handler first
    webSocket.close();
};

