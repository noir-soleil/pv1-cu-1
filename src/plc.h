#ifndef __INCLUDED_PLC_H__
#define __INCLUDED_PLC_H__

#include <stdint.h>
#include <stdbool.h>

enum plc_mode_t {
	plcMode_NORMAL = 0,
	plcMode_BOOT
};

struct plc_count_t {
	long tx, rxok, part, none;
};

enum plc_priority_t {
	plcPrio_HEATER = 0,
	plcPrio_INV,
};

enum plc_boot_target_t {
	plcBoot_INV = 0,
	plcBoot_LCC
};

static inline void plc_set_target(void* cmdBuf, enum plc_boot_target_t t)
{
	char *b = (char*)cmdBuf;
	if (t == plcBoot_INV) b[56] = 0x00;
	else                  b[56] = 0x01;
};

extern int plc_rx_T;
extern int plc_rx_Tsrc;
extern float plc_rx_Pwr;
extern float plc_rx_Cur;
extern int plc_rx_Ready;

extern int plc_tx_Pwr;
extern int plc_tx_Enable;
extern bool plc_tx_FakePwr;
extern enum plc_priority_t plc_tx_Priority;

extern enum plc_mode_t plc_TX_mode;
extern enum plc_mode_t plc_RX_mode;
extern unsigned char plc_TX_boot_data[88];
extern bool plc_TX_boot_new_data;
extern int plc_st7540_state;
extern uint16_t plc_RX_boot_Cmd;
extern uint16_t plc_RX_boot_Ret;
extern unsigned int plc_RX_boot_Bit;
extern struct plc_count_t plc_count;

extern void plc_handler();

#endif /* ndef __INCLUDED_PLC_H__ */
