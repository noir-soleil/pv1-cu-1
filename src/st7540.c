#include "st7540.h"
#include "sdkconfig.h"

#include <stdint.h>
#include "esp_system.h"
#include "esp_log.h"
#include "esp_event.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "hal/uart_hal.h"
#include "hal/gpio_hal.h"
#include "driver/periph_ctrl.h"
#include "soc/uart_periph.h"
#include "esp_rom_sys.h"
#include "esp_rom_gpio.h"

#include "am1805.h"

#define PLC_ENABLED CONFIG_PLC_ENABLED

#define PIN_ST7540_RXTX CONFIG_ST7540_RXTX_GPIO_NUM
#define PIN_ST7540_RXD CONFIG_ST7540_RXD_GPIO_NUM
#define PIN_ST7540_TXD CONFIG_ST7540_TXD_GPIO_NUM
#define PIN_ST7540_CLR_T CONFIG_ST7540_CLR_T_GPIO_NUM
#define PIN_ST7540_BU_THERM CONFIG_ST7540_BU_THERM_GPIO_NUM

#define ST7540_UART_NUM 1

#if PLC_ENABLED
uart_hal_context_t st7540_uart_hal = {
	.dev = UART_LL_GET_HW(ST7540_UART_NUM)
};

static void IRAM_ATTR st7540_uart_isr_handler(void *param);
#endif /* PLC_ENABLED */

#if defined(UART_PERIPH_SIGNAL)
#	define UART_TX_SIG UART_PERIPH_SIGNAL(ST7540_UART_NUM, SOC_UART_TX_PIN_IDX)
#	define UART_RX_SIG UART_PERIPH_SIGNAL(ST7540_UART_NUM, SOC_UART_RX_PIN_IDX)
#	define UART_RTS_SIG UART_PERIPH_SIGNAL(ST7540_UART_NUM, SOC_UART_RTS_PIN_IDX)
#else
#	define UART_TX_SIG uart_periph_signal[ST7540_UART_NUM].tx_sig
#	define UART_RX_SIG uart_periph_signal[ST7540_UART_NUM].rx_sig
#	define UART_RTS_SIG uart_periph_signal[ST7540_UART_NUM].rts_sig
#endif

static const char *TAGP = "plc";

esp_err_t st7540_init()
{
    ESP_LOGI(TAGP, "executing %s() on CORE%i", __func__, xPortGetCoreID());
#if PLC_ENABLED

	gpio_set_level(PIN_ST7540_RXTX, 1); // RX
	gpio_set_direction(PIN_ST7540_RXTX, GPIO_MODE_OUTPUT);

	gpio_set_direction(PIN_ST7540_RXD, GPIO_MODE_INPUT);
	gpio_set_level(PIN_ST7540_TXD, 1);
	gpio_set_direction(PIN_ST7540_TXD, GPIO_MODE_OUTPUT);
	gpio_set_direction(PIN_ST7540_CLR_T, GPIO_MODE_INPUT);
	gpio_set_direction(PIN_ST7540_BU_THERM, GPIO_MODE_INPUT);

	am1805_set_plc_uart_spi(0); // SPI
	vTaskDelay(10 / portTICK_RATE_MS);

	am1805_set_plc_reg_data(0); // DATA
	vTaskDelay(10 / portTICK_RATE_MS);

	am1805_set_plc_reg_data(1); // REG

	uint32_t ctrl_reg_read = 0;

	/* Read control register */
	while (gpio_get_level(PIN_ST7540_CLR_T));
	for (int i = 23; i >= 0; i--) {
		while (!gpio_get_level(PIN_ST7540_CLR_T));
		ctrl_reg_read |= (gpio_get_level(PIN_ST7540_RXD) ? 1 : 0) << i;
		while (gpio_get_level(PIN_ST7540_CLR_T));
	}

    ESP_LOGI(TAGP, "Control register: 0x%06x", ctrl_reg_read);

	for (;;) {
		vTaskDelay(10 / portTICK_RATE_MS);

		uint32_t ctrl_reg =
			(6 << 0) // 110kHz
			| (2 << 3) // 2400 baud
			| (0 << 5) // Deviation 0.5
			| (0 << 6) // Watchdog disabled
			| (3 << 7) // Transmission timeout not used
			| (0 << 9) // Frequency detection time 0.5 ms
			| (2 << 12) // Carrier detection without conditioning
			| (1 << 14) // Asynchronous Mains Interfacing
			| (3 << 15) // Output Clock OFF
			| (1 << 17) // Output voltage level Freeze Disabled
			| (0 << 18) // Header recognition Disabled
			| (0 << 19) // Frame length count Disabled
			| (1 << 20) // Header length 16 bit
			| (0 << 21) // Extended register Disabled
			| (0 << 22) // Normal sensitivity
			| (0 << 23) // Input Filter Disabled
			;

		gpio_set_level(PIN_ST7540_TXD, ((ctrl_reg >> 23) & 0x1));
		gpio_set_level(PIN_ST7540_RXTX, 1); // RX
		am1805_set_plc_reg_data(0); // DATA
		vTaskDelay(10 / portTICK_RATE_MS);
		gpio_set_level(PIN_ST7540_RXTX, 0); // TX
		am1805_set_plc_reg_data(1); // REG

		/* Write control register */
		while (gpio_get_level(PIN_ST7540_CLR_T));
		for (int i = 23; i >= 0; i--) {
			gpio_set_level(PIN_ST7540_TXD, ((ctrl_reg >> i) & 0x1));
			while (!gpio_get_level(PIN_ST7540_CLR_T));
			while (gpio_get_level(PIN_ST7540_CLR_T));
		}
		gpio_set_level(PIN_ST7540_RXTX, 1); // RX
		am1805_set_plc_reg_data(0); // DATA

		vTaskDelay(10 / portTICK_RATE_MS);

		ctrl_reg_read = 0;

		gpio_set_level(PIN_ST7540_RXTX, 1); // RX
		am1805_set_plc_reg_data(0); // DATA
		vTaskDelay(10 / portTICK_RATE_MS);
		am1805_set_plc_reg_data(1); // REG

		/* Read control register */
		while (gpio_get_level(PIN_ST7540_CLR_T));
		for (int i = 23; i >= 0; i--) {
			while (!gpio_get_level(PIN_ST7540_CLR_T));
			ctrl_reg_read |= (gpio_get_level(PIN_ST7540_RXD) ? 1 : 0) << i;
			while (gpio_get_level(PIN_ST7540_CLR_T));
		}

		ESP_LOGI(TAGP, "Control register: 0x%06x", ctrl_reg_read);
		if (ctrl_reg == ctrl_reg_read) {
			break;
		}

		ESP_LOGE(TAGP, "Control register written incorrectly: wr 0x%06x, rd 0x%06x", ctrl_reg, ctrl_reg_read);
	}

	am1805_set_plc_reg_data(0); // DATA
	am1805_set_plc_uart_spi(1); // UART
	vTaskDelay(10 / portTICK_RATE_MS);
#endif /* PLC_ENABLED */

	return ESP_OK;
}

esp_err_t st7540_uart_init()
{
    ESP_LOGI(TAGP, "executing %s() on CORE%i", __func__, xPortGetCoreID());
#if PLC_ENABLED

	// Module enable
	periph_module_enable(uart_periph_signal[ST7540_UART_NUM].module);
#if SOC_UART_REQUIRE_CORE_RESET
	uart_hal_set_reset_core(&st7540_uart_hal, true);
	periph_module_reset(uart_periph_signal[ST7540_UART_NUM].module);
	uart_hal_set_reset_core(&st7540_uart_hal, false);
#else
	periph_module_reset(uart_periph_signal[ST7540_UART_NUM].module);
#endif
    uart_hal_init(&st7540_uart_hal, ST7540_UART_NUM);

    // Install UART driver (we don't need an event queue here)
    // In this example we don't even use a buffer for sending data.
    uart_hal_disable_intr_mask(&st7540_uart_hal, UART_LL_INTR_MASK);
    uart_hal_clr_intsts_mask(&st7540_uart_hal, UART_LL_INTR_MASK);
    ESP_ERROR_CHECK(esp_intr_alloc(uart_periph_signal[ST7540_UART_NUM].irq, ESP_INTR_FLAG_IRAM, &st7540_uart_isr_handler, NULL, NULL));

    // Configure UART parameters
    uart_hal_set_sclk(&st7540_uart_hal, UART_SCLK_APB);
    uart_hal_set_baudrate(&st7540_uart_hal, 2400);
    uart_hal_set_parity(&st7540_uart_hal, UART_PARITY_DISABLE);
    uart_hal_set_data_bit_num(&st7540_uart_hal, UART_DATA_7_BITS);
    uart_hal_set_stop_bits(&st7540_uart_hal, UART_STOP_BITS_1);
    uart_hal_set_tx_idle_num(&st7540_uart_hal, 0);
    uart_hal_set_hw_flow_ctrl(&st7540_uart_hal, UART_HW_FLOWCTRL_DISABLE, 122);
    uart_hal_rxfifo_rst(&st7540_uart_hal);
    uart_hal_txfifo_rst(&st7540_uart_hal);

    // Set UART pins as per KConfig settings
	gpio_hal_iomux_func_sel(GPIO_PIN_MUX_REG[PIN_ST7540_TXD], PIN_FUNC_GPIO);
	gpio_set_level(PIN_ST7540_TXD, 1);
	esp_rom_gpio_connect_out_signal(PIN_ST7540_TXD, UART_TX_SIG, 0, 0);

	gpio_hal_iomux_func_sel(GPIO_PIN_MUX_REG[PIN_ST7540_RXD], PIN_FUNC_GPIO);
	gpio_set_pull_mode(PIN_ST7540_RXD, GPIO_PULLUP_ONLY);
	gpio_set_direction(PIN_ST7540_RXD, GPIO_MODE_INPUT);
	esp_rom_gpio_connect_in_signal(PIN_ST7540_RXD, UART_RX_SIG, 0);

	gpio_hal_iomux_func_sel(GPIO_PIN_MUX_REG[PIN_ST7540_RXTX], PIN_FUNC_GPIO);
	gpio_set_direction(PIN_ST7540_RXTX, GPIO_MODE_OUTPUT);
	esp_rom_gpio_connect_out_signal(PIN_ST7540_RXTX, UART_RTS_SIG, 0, 0);
	uart_hal_inverse_signal(&st7540_uart_hal, UART_SIGNAL_RTS_INV);

    // Set RS485 half duplex mode
    uart_hal_set_mode(&st7540_uart_hal, UART_MODE_RS485_HALF_DUPLEX);
#endif /* PLC_ENABLED */

	return ESP_OK;
}

esp_err_t st7540_uart_send(const char *buffer, uint32_t len)
{
#if PLC_ENABLED
	if (len == 0) return ESP_OK;
	uint32_t tx_len;

	uart_hal_set_rts(&st7540_uart_hal, 0);
	esp_rom_delay_us(400 + 3200); // wait some time Trxtx + Talc
	if (gpio_get_level(PIN_ST7540_BU_THERM)) {
		uart_hal_set_rts(&st7540_uart_hal, 1);
		return ESP_ERR_NOT_SUPPORTED;
	}

	//uart_hal_set_rts(&st7540_uart_hal, 0);
	uart_hal_ena_intr_mask(&st7540_uart_hal, UART_INTR_TX_DONE);
    uart_hal_write_txfifo(&st7540_uart_hal, (const uint8_t *) buffer, len, (uint32_t *)&tx_len);

	if (tx_len == len) return ESP_OK;

	return ESP_ERR_TIMEOUT;
#else
	return ESP_OK;
#endif /* PLC_ENABLED */
}

int st7540_uart_recv(void* buf, int32_t len)
{
#if PLC_ENABLED
	int rd_len = uart_hal_get_rxfifo_len(&st7540_uart_hal);
	if (rd_len != 0) {
		if (rd_len > len) rd_len = len;
		uart_hal_read_rxfifo(&st7540_uart_hal, (uint8_t*)buf, &rd_len);
	}
	return rd_len;
#else
	return 0;
#endif /* PLC_ENABLED */
}

size_t st7540_uart_rxsize()
{
#if PLC_ENABLED
	return (size_t)uart_hal_get_rxfifo_len(&st7540_uart_hal);
#else
	return 0;
#endif /* PLC_ENABLED */
}

void st7540_uart_flush()
{
#if PLC_ENABLED
	uart_hal_rxfifo_rst(&st7540_uart_hal);
#endif /* PLC_ENABLED */
}

bool st7540_uart_tx_active()
{
#if PLC_ENABLED
	return !uart_hal_is_tx_idle(&st7540_uart_hal);
#else
	return false;
#endif /* PLC_ENABLED */
}

#if PLC_ENABLED
static void IRAM_ATTR st7540_uart_isr_handler(void *param)
{
    uint32_t uart_intr_status = 0;
    while (1) {
        // The `continue statement` may cause the interrupt to loop infinitely
        // we exit the interrupt here
        uart_intr_status = uart_hal_get_intsts_mask(&st7540_uart_hal);
        //Exit form while loop
        if (uart_intr_status == 0) {
            break;
        }

        if (uart_intr_status & UART_INTR_TX_DONE) {
			uart_hal_clr_intsts_mask(&st7540_uart_hal, UART_INTR_TX_DONE);
            if (uart_hal_is_tx_idle(&st7540_uart_hal) != true) {
                // The TX_DONE interrupt is triggered but transmit is active
                // then postpone interrupt processing for next interrupt
            } else {
                // Workaround for RS485: If the RS485 half duplex mode is active
                // and transmitter is in idle state then reset received buffer and reset RTS pin
                // skip this behavior for other UART modes
                uart_hal_disable_intr_mask(&st7540_uart_hal, UART_INTR_TX_DONE);
				uart_hal_rxfifo_rst(&st7540_uart_hal);
				uart_hal_set_rts(&st7540_uart_hal, 1);
            }
        } else {
            uart_hal_clr_intsts_mask(&st7540_uart_hal, uart_intr_status); /*simply clear all other intr status*/
        }
    }
}
#endif /* PLC_ENABLED */

/* vim: set ts=4 sw=4 noet nowrap: */
