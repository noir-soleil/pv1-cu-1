#ifndef _INCLUDED_TER_U12B_H_
#	define _INCLUDED_TER_U12B_H_

#include "font.h"

extern const struct font font_ter_u12b_bdf;

#endif /* ndef _INCLUDED_TER_U12B_H_ */
