#ifndef _INCLUDED_WIFI_H_
#	define _INCLUDED_WIFI_H_

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_http_server.h"
#include "esp_wifi_types.h"

esp_err_t wifi_init(void);
void wifi_register_handlers(httpd_config_t *cfg);
void wifi_register_uris(httpd_handle_t server);
const wifi_ap_config_t *wifi_get_ap_config(void);
const wifi_sta_config_t *wifi_get_sta_config(void);
extern esp_ip4_addr_t wifi_sta_ip4;


#endif /* ndef _INCLUDED_WIFI_H_ */

/* vim: set ts=4 sw=4 noet nowrap cin ai: */
