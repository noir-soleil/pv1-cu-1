#ifndef _INCLUDED_ADC_H_
#	define _INCLUDED_ADC_H_

#include "esp_system.h"
#include <stdint.h>

esp_err_t adc_init();

size_t adc_get_fbuf_size();
float* adc_get_fbuf(int tag);
void adc_give_fbuf(int tag);

const int32_t* adc_get_v_buffer();
const int32_t* adc_get_c_buffer();
bool adc_buffer_full();
uint32_t adc_buffer_size();
void adc_restart();
void adc_task(void *parameters);
float adc_get_pwr();

#endif /* ndef _INCLUDED_ADC_H_ */

/* vim: set ts=4 sw=4 noet nowrap: */
