#ifndef _INCLUDED_ST7540_H_
#	define _INCLUDED_ST7540_H_

#include "esp_system.h"

esp_err_t st7540_init();
esp_err_t st7540_uart_init();
esp_err_t st7540_uart_send(const char *buffer, uint32_t len);
int st7540_uart_recv(void* buf, int32_t len);
size_t st7540_uart_rxsize();
void st7540_uart_flush();
bool st7540_uart_tx_active();

#endif /* ndef _INCLUDED_ST7540_H_ */

/* vim: set ts=4 sw=4 noet nowrap: */
