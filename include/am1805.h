#ifndef _INCLUDED_AM1805_H_
#	define _INCLUDED_AM1805_H_

#include "esp_system.h"

esp_err_t am1805_init();
esp_err_t am1805_set_bit(uint8_t address, int num, int value);
esp_err_t am1805_set_plc_reg_data(int value);
esp_err_t am1805_set_plc_uart_spi(int value);

#endif /* ndef _INCLUDED_AM1805_H_ */

/* vim: set ts=4 sw=4 noet nowrap: */
